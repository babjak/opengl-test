#ifndef STUFF_H_INCLUDED
#define STUFF_H_INCLUDED

#include <GL/glfw.h>
//#include <AntTweakBar.h>
//#define PU_USE_GLFW
//#include <puGLFW.h>

#include "WindowParam.h"
#include "flags.h"
#include "gui.h"


/////////////////////////////////////////////////
//
// Value declaration
//
/////////////////////////////////////////////////


/////////////////////////////////////////////////
//
// Parameter structure
//
/////////////////////////////////////////////////
class CWHParam :
    public virtual CWinParam,
    public virtual CFlags
{
    public:
        GLFWvidmode     current_vidmode;

    CWHParam()
    {

    }

	bool CWHParam::operator==(const CWHParam &other) const 
	{
		return 
			*((CFlags*) &other) == *((CFlags*) this)
			&&
			*((CWinParam*) &other) == *((CWinParam*) this);
	}

	bool CWHParam::operator!=(const CWHParam &other) const 
	{
		return !(*this == other);
	}
};


/////////////////////////////////////////////////
//
// Functions implemented in .cpp visible
// from outside
//
/////////////////////////////////////////////////
// Callback functions
void        GLFWCALL windowsize_cbfun		( int width, int height );
void        GLFWCALL key_cbfun				( int key_id, int key_state );
void        GLFWCALL char_cbfun				( int key_id, int key_state );
void        GLFWCALL mousepos_cbfun			( int x, int y );
void        GLFWCALL mousepos_cbGUIfun		( int x, int y );
void        GLFWCALL mousebutton_cbfun		( int button_id, int button_state );
void        GLFWCALL mousebutton_cbGUIfun	( int button_id, int button_state );
void        GLFWCALL mousewheel_cbfun		( int wheel_pos );


//void		SetMouseVisibilityRelatedStuff( );
int         InitGL      ( );
int         OpenWindow  ( );
void        InitMisc    ( );


/////////////////////////////////////////////////
//
// Functions needed, but implemented somewhere else...
//
/////////////////////////////////////////////////
CWHParam&   GetWHParam( );
void		InitCallback( );
void		SetMouseCallbacks(char GUI);

#endif // STUFF_H_INCLUDED
