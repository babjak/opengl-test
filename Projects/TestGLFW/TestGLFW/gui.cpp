#include "gui.h"

#ifdef USE_GLGOOEY
using namespace Gooey;

Console::Console()
		:
        Panel(Core::Rectangle(50, 50, 350, 150), 0),
        ExitButton_(0, "Exit"),
        ResumeButton_(0, "Resume")
{
	// Leave this out and the user will not be able to drag the window around by its frame
//        enableMovement();

	// set the "done" buttton's size
    ExitButton_.setSize(Core::Vector2(55, 30));
    ResumeButton_.setSize(Core::Vector2(55, 30));

    // wire up the signal/slot connections - pressing Return in the edit field should have
    // the same effect as pressing the done button
    ExitButton_.pressed.connect(this, &Console::ExitButtonPressed);
    ResumeButton_.pressed.connect(this, &Console::ResumeButtonPressed);

    GridLayouter* layouter = new GridLayouter(1, 2, 20, 20);
    setLayouter(layouter);

    // put the controls in the panel
    this->addChildWindow(&ExitButton_);
    this->addChildWindow(&ResumeButton_);

        // arrange the panel's children according to the default flow layouter
    this->arrangeChildren();
}


void Console::ExitButtonPressed()
{
    CGUIParam& p = GetGUIParam();

	SetFlag( p.status_flags, STATUS_EXIT );
}


void Console::ResumeButtonPressed()
{
    CGUIParam& p = GetGUIParam();

    ResetFlag( p.status_flags, STATUS_MAIN_MENU );
	ShowExitDialog( 0 );

	SetMouseVisibilityRelatedStuff( );
}

#endif

void InitGUI()
{
	CGUIParam& p = GetGUIParam();

#ifdef USE_GLGOOEY
    // **********************************************************************************************
    //  Initialize GLGooey - The parameters are:
    //   - The default font to use
    //   - The three modifier key functions defined above
    // **********************************************************************************************
    WindowManager::instance().initialize("../data/accid___.ttf", isShiftPressed, isAltPressed, isCtrlPressed);

    // **********************************************************************************************
    //   Create an instance of the GLGooey window defined in the Console class and add it to the
    //  window manager. Once it has been added it will be drawn every time
    //  WindowManager::instance().update() is called
    // **********************************************************************************************
    WindowManager::instance().addWindow(&(p.console));
#endif

#ifdef USE_ANTTWEAKBAR
	    // Initialize AntTweakBar
	TwInit(TW_OPENGL, NULL);
#endif
}


void TW_CALL LightEnabledSetCallback(const void *value, void *clientData)
{ 
	char	params[100];

	CGUIParam& p = GetGUIParam();
	int i = (int) clientData;

	p.light_enabled[i] = *(const char *)value;  // for instance

	if (p.light_enabled[i])
		sprintf(params, "LightTweakBar/%d. label='%d.*' ", i, i/*, p.light_enabled[i] ? "light" : "dark"*/);
	else
		sprintf(params, "LightTweakBar/%d. label='%d.'", i, i/*, p.light_enabled[i] ? "light" : "dark"*/);

	TwDefine(params);
}

void TW_CALL LightEnabledGetCallback(void *value, void *clientData)
{ 
	CGUIParam& p = GetGUIParam();
	int i = (int) clientData;

    *(char *)value = p.light_enabled[i];  // for instance
}


void CreateGUI()
{
	CGUIParam& p = GetGUIParam();

#ifdef USE_ANTTWEAKBAR
// General
        // Menu and Parameter bar
        TwDefine(" GLOBAL fontresizable=false "); // font cannot be resized
        TwDefine(" GLOBAL fontsize=3 "); // use large font
        TwDefine(" GLOBAL contained=true "); // bars cannot move outside of the window


// Parameters Bar
    p.bar = TwNewBar("TweakBar");
    TwDefine(" GLOBAL help='This example shows how to integrate AntTweakBar with GLFW and OpenGL.' "); // Message added to the help bar.
	TwDefine("TweakBar position='20 30' label='Params' ");

    // Add 'speed' to 'bar': it is a modifable (RW) variable of type TW_TYPE_DOUBLE. Its key shortcuts are [s] and [S].
    TwAddVarRW(p.bar, "speed", TW_TYPE_DOUBLE, &p.angular_vel, 
               " label='Rot speed' min=0 max=2000 step=5 help='Rotation speed (turns/second)' ");

    TwAddVarRW(p.bar, "Specular", TW_TYPE_COLOR4F, &p.mat_specular, "group='Material' ");
    TwAddVarRW(p.bar, "Ambient", TW_TYPE_COLOR4F, &p.mat_ambient, "group='Material' ");
    TwAddVarRW(p.bar, "Diffuse", TW_TYPE_COLOR4F, &p.mat_diffuse, "group='Material' ");
    TwAddVarRW(p.bar, "Shininess", TW_TYPE_FLOAT, &p.mat_shininess, 
               "group='Material' label='Shininess' min=0 max=200 step=0.1 ");

	TwDefine("TweakBar/Material opened=false");


	TwAddVarRW(p.bar, "Enabled", TW_TYPE_BOOL8, &p.Axis_enabled, "group='Axis' ");
	TwAddVarRW(p.bar, "Length", TW_TYPE_FLOAT, &p.Axis_length, 
               "group='Axis' label='Length' min=-500 max=500 step=0.1");
	TwAddVarRW(p.bar, "Width", TW_TYPE_UINT8, &p.Axis_width, 
               "group='Axis' label='Width' min=1 max=40 step=1");

	TwDefine("TweakBar/Axis opened=false");


// Lights Bar
    p.Lightbar = TwNewBar("LightTweakBar");

	TwDefine("LightTweakBar position='680 30' label='Lights'");

	TwAddVarRW(p.Lightbar, "Lighting", TW_TYPE_BOOL8, &p.lighting_enabled, "");
	TwAddVarRW(p.Lightbar, "Colour-driven materials", TW_TYPE_BOOL8, &p.CDM_enabled, "");


	TwStructMember PosStructMembers[] = {
		{ "X", TW_TYPE_FLOAT, 0*sizeof(GLfloat), "step=0.01" },
		{ "Y", TW_TYPE_FLOAT, 1*sizeof(GLfloat), "step=0.01" },
		{ "Z", TW_TYPE_FLOAT, 2*sizeof(GLfloat), "step=0.01" },
		{ "W", TW_TYPE_FLOAT, 3*sizeof(GLfloat), "step=0.01" }
	};

	TwType PosStructType = TwDefineStruct("PosStructType", PosStructMembers, 4, sizeof(GLfloat4), NULL, NULL);

	TwStructMember AttStructMembers[] = {
		{ "Constant", TW_TYPE_FLOAT, 0*sizeof(GLfloat), "step=0.001 min=0.001 max=200" },
		{ "Linear", TW_TYPE_FLOAT, 1*sizeof(GLfloat), "step=0.001 min=0.001 max=200" },
		{ "Quadratic", TW_TYPE_FLOAT, 2*sizeof(GLfloat), "step=0.001 min=0.001 max=200" }
	};

	TwType AttStructType = TwDefineStruct("AttStructType", AttStructMembers, 3, sizeof(GLfloat3), NULL, NULL);


	for (unsigned int i = 0; i < p.maxLights; i++)
	{
		char	name[100];
		char	params[100];

		sprintf(name, "LightEnabled%d", i);
		sprintf(params, "label='Enabled' group='%d.' ", i);
//		TwAddVarRW(p.Lightbar, name, TW_TYPE_BOOL8, &(p.light_enabled[i]), params);
		TwAddVarCB(p.Lightbar, name, TW_TYPE_BOOL8, LightEnabledSetCallback, LightEnabledGetCallback, (void*) i, params);

		sprintf(name, "Pos%d", i);
		sprintf(params, "label='Position' group='%d.'", i);
		TwAddVarRW(p.Lightbar, name, PosStructType, &(p.light_position[i]), params);

		sprintf(name, "Spec%d", i);
		sprintf(params, "noalpha label='Specular' group='%d.'", i);
		TwAddVarRW(p.Lightbar, name, TW_TYPE_COLOR4F, &(p.light_specular[i]), params);

		sprintf(name, "Ambient%d", i);
		sprintf(params, "noalpha label='Ambient' group='%d.'", i);
		TwAddVarRW(p.Lightbar, name, TW_TYPE_COLOR4F, &(p.light_ambient[i]), params);

		sprintf(name, "Diff%d", i);
		sprintf(params, "noalpha label='Diffuse' group='%d.'", i);
		TwAddVarRW(p.Lightbar, name, TW_TYPE_COLOR4F, &(p.light_diffuse[i]), params);

		sprintf(name, "Att%d", i);
		sprintf(params, "label='Attenuation' group='%d.'", i);
		TwAddVarRW(p.Lightbar, name, AttStructType, &(p.light_att[i]), params);

		if (p.light_enabled[i])
			sprintf(params, "LightTweakBar/%d. opened=false label='%d.*' ", i, i/*, p.light_enabled[i] ? "light" : "dark"*/);
		else
			sprintf(params, "LightTweakBar/%d. opened=false label='%d.'", i, i/*, p.light_enabled[i] ? "light" : "dark"*/);

		TwDefine(params);
	}
#endif
}


void ResizeWindowGUI(int width, int height)
{
	CGUIParam& p = GetGUIParam();

#ifdef USE_GLGOOEY
	p.console.setWindowRectangle( Gooey::Core::Rectangle( (width - 300)/2, (height - 100)/2, (width - 300)/2+300, (height - 100)/2+100 ) );
#endif
}


void ShowExitDialog(unsigned char show)
{
	CGUIParam& p = GetGUIParam();

#ifdef USE_GLGOOEY
	if (show)
	{
		p.console.show();
	}
	else
	{
		p.console.hide();
	}
#endif
}

void ShowParamBars(unsigned char show)
{
	CGUIParam& p = GetGUIParam();

#ifdef USE_ANTTWEAKBAR
	if (show)
	{
		TwDefine("TweakBar visible=true");
		TwDefine("LightTweakBar visible=true");
	}
	else
	{
		TwDefine("TweakBar visible=false");
		TwDefine("LightTweakBar visible=false");
	}
#endif
}