#include "gfx.h"
#include "MathStuff.h"


/////////////////////////////////////////////////
//
// Value declaration
//
/////////////////////////////////////////////////
#define OBJECT_GROUND_SURFACE       1
#define OBJECT_ROTATING_TRIANGLES   2
//#define OBJECT_HALO					3

#include "FontRaster.h"
GLuint fontOffset;


/*******************************************************************************
*******************************************************************************
**	Create OpenGL command lists to draw objects later on.
**
**	@param  TODO  TODO
**
*******************************************************************************/
void makeRasterFont( )
{
    GLuint i;
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    fontOffset = glGenLists (128);
    for (i = 32; i < 127; i++) {
        glNewList(i+fontOffset, GL_COMPILE);
            glBitmap(8, 13, 0.0, 2.0, 10.0, 0.0, rasters[i-32]);
        glEndList();
    }
}


void InitGFX()
{
    CGFXParam& p = GetGFXParam();
	
	glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//    glEnable(GL_CULL_FACE);
    glEnable(GL_NORMALIZE);


	// Makes 3D work when something is in front of something else
    // This might even be a default value...
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    // Dithering me no likey...
    glDisable( GL_DITHER );

    // Allow each vertex to have individual colors
    glShadeModel (GL_SMOOTH);

	// Get the max number of lights allowed by the graphic card
    glGetIntegerv(GL_MAX_LIGHTS, &p.maxLights);

	p.light_enabled = (char*) malloc( sizeof(char)*p.maxLights );				
	p.light_specular = (GLfloat4*) malloc( sizeof(GLfloat)*4*p.maxLights );				
	p.light_ambient = (GLfloat4*) malloc( sizeof(GLfloat)*4*p.maxLights );
	p.light_diffuse = (GLfloat4*) malloc( sizeof(GLfloat)*4*p.maxLights );
	p.light_position = (GLfloat4*) malloc( sizeof(GLfloat)*4*p.maxLights );
	p.light_att = (GLfloat3*) malloc( sizeof(GLfloat)*3*p.maxLights );

	for (unsigned int i = 0; i < p.maxLights; i++)
	{
		p.light_enabled[i] = 0;

		p.light_position[i][0] = 0.0;
		p.light_position[i][1] = 5.0;
		p.light_position[i][2] = 0.0;
		p.light_position[i][3] = 1.0;

		p.light_specular[i][0] = 1.0;
		p.light_specular[i][1] = 1.0;
		p.light_specular[i][2] = 1.0;
		p.light_specular[i][3] = 1.0;

		p.light_diffuse[i][0] = 1.0;
		p.light_diffuse[i][1] = 1.0;
		p.light_diffuse[i][2] = 1.0;
		p.light_diffuse[i][3] = 1.0;

		p.light_ambient[i][0] = 1.0;
		p.light_ambient[i][1] = 1.0;
		p.light_ambient[i][2] = 1.0;
		p.light_ambient[i][3] = 1.0;

		p.light_att[i][0] = 0.001;
		p.light_att[i][1] = 0.5;
		p.light_att[i][2] = 0.015;
	}
	p.light_enabled[0] = 1;


//    glEnable(GL_LIGHTING);
//	glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_DIFFUSE);
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
}


/*******************************************************************************
*******************************************************************************
**	Create OpenGL command lists to draw objects later on.
**
**	@param  TODO  TODO
**
*******************************************************************************/
void CalculateCam( double time_delta )
{
    CGFXParam& p = GetGFXParam();


    //rotation
    p.theta -= p.cam_angular_vel*(p.mouse_y - p.mouse_y_previous);
    p.fi -= p.cam_angular_vel*(p.mouse_x - p.mouse_x_previous);

    p.theta = MIN(p.theta, 90.0);
    p.theta = MAX(p.theta, -90.0);

    glRotatef(-p.theta, 1.0f, 0.0f, 0.0f);
    glRotatef(-p.fi, 0.0f, 1.0f, 0.0f);


    p.mouse_x_previous = p.mouse_x;
    p.mouse_y_previous = p.mouse_y;


    //movement
    vec3    right_unit;
    vec3    forward_unit;

    p.cam_o.x = -cos(radians(p.theta)) * sin(radians(p.fi));
    p.cam_o.y = sin(radians(p.theta));
    p.cam_o.z = -cos(radians(p.theta)) * cos(radians(p.fi));

    forward_unit = p.cam_o;
    right_unit = normalize( cross( vec3(p.cam_o.x, 0.0, p.cam_o.z), vec3(0.0f, 1.0f, 0.0f) ) );

    vec3 tr_vec(0.0, 0.0, 0.0);

    if ( GetFlag( p.movement_flags, UP_BIT ) )
        tr_vec += forward_unit;
    if ( GetFlag( p.movement_flags, DOWN_BIT ) )
        tr_vec -= forward_unit;
    if ( GetFlag( p.movement_flags, RIGHT_BIT ) )
        tr_vec += right_unit;
    if ( GetFlag( p.movement_flags, LEFT_BIT ) )
        tr_vec -= right_unit;

    if (
        ( GetFlag(p.movement_flags, UP_BIT) != GetFlag(p.movement_flags, DOWN_BIT) )
        ||
        ( GetFlag(p.movement_flags, RIGHT_BIT) != GetFlag(p.movement_flags, LEFT_BIT) )
    )
        tr_vec = normalize( tr_vec );


    tr_vec *= p.cam_speed * time_delta;
    p.cam_p += tr_vec;
    tr_vec = p.cam_p;
    tr_vec *= -1;

    glTranslatef( tr_vec.x, tr_vec.y, tr_vec.z );
}


void DrawAxis()
{
    CGFXParam& p = GetGFXParam();

//    glMatrixMode( GL_MODELVIEW );
//    glLoadIdentity();
	glLineWidth( p.Axis_width );

    glBegin( GL_LINES );
    glColor3f(1.0f, 0.0f, 0.0f );

    glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(p.Axis_length, 0.0f, 0.0f);
    glEnd();

    glBegin( GL_LINES );
    glColor3f(0.0f, 1.0f, 0.0f );

    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, p.Axis_length, 0.0f);
    glEnd();

    glBegin( GL_LINES );
    glColor3f(0.0f, 0.0f, 1.0f );

    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, p.Axis_length);
    glEnd();

	glLineWidth( 1 );
}

/*
// Subroutine used to build halo display list
void DrawSubdivHaloZ(float x, float y, float z, float radius, int subdiv)
{
    glBegin(GL_TRIANGLE_FAN);
    //glNormal3f(0, 0, 1);
    glColor4f(1, 1, 1, 1);
    glVertex3f(x, y, z);
    for( int i=0; i<=subdiv; ++i )
    {
        glColor4f(1, 1, 1, 0);
		glVertex3f(x+radius*(float)cos(FLOAT_2PI*(float)i/subdiv), y+radius*(float)sin(FLOAT_2PI*(float)i/subdiv), z);
    }
    glEnd();
}
*/

void DrawLightStars()
{
    CGFXParam& p = GetGFXParam();

	for (unsigned int i = 0; i < p.maxLights; i++)
    {
		if( p.light_enabled[i] )
		{
			glPushAttrib( GL_ALL_ATTRIB_BITS );
			glPushMatrix();
//			glLoadIdentity();

			glDepthMask(GL_FALSE);
			glEnable (GL_BLEND);
			glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glDisable(GL_LIGHTING);
			glDisable(GL_COLOR_MATERIAL);

//			DrawSubdivHaloZ(p.light_position[i][0]/p.light_position[i][3], p.light_position[i][1]/p.light_position[i][3], p.light_position[i][2]/p.light_position[i][3], 1, 6);

			vec3 temp1 = p.cam_p - glm::vec3(p.light_position[i][0]/p.light_position[i][3], p.light_position[i][1]/p.light_position[i][3], p.light_position[i][2]/p.light_position[i][3]);

			vec3 temp2 = eucl2polar(temp1);


			glTranslatef(p.light_position[i][0], p.light_position[i][1], p.light_position[i][2]/*, p.light_position[i][3]*/);
//			glRotatef(45, 1, 0, 0);
			glRotatef(temp2.z/FLOAT_2PI*360, 0, 1, 0);
			glRotatef(temp2.y/FLOAT_2PI*360 - 90, 1, 0, 0);

//			DrawSubdivHaloZ(0, 0, 0, 1, 5);
			float radius = 0.4;
			int subdiv = 5;

			glBegin(GL_TRIANGLE_FAN);
			glColor4f(p.light_diffuse[i][0], p.light_diffuse[i][1], p.light_diffuse[i][2], 1);
			glVertex3f(0, 0, 0);
			for( int j=0; j<=subdiv; ++j )
			{
				glColor4f(p.light_diffuse[i][0], p.light_diffuse[i][1], p.light_diffuse[i][2], 0);
				glVertex3f(radius*(float)cos(FLOAT_2PI*(float)j/subdiv), radius*(float)sin(FLOAT_2PI*(float)j/subdiv), 0);
			}
			glEnd();


			glPopMatrix();
			glPopAttrib();
		}
	}

	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}


/*******************************************************************************
*******************************************************************************
**	Create OpenGL command lists to draw objects later on.
**
**	@param  TODO  TODO
**
*******************************************************************************/
void Draw( double time_delta )
{
    CGFXParam& p = GetGFXParam();

// Clear stuff
    glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

// Translation and rotation of stuff
    glMatrixMode( GL_MODELVIEW );

    glLoadIdentity();
	glDisable(GL_LIGHTING);

	// Cam
    CalculateCam( time_delta );

// Draw coordinate system
	if (p.Axis_enabled)
		DrawAxis();

//	DrawLightStars();

// Lighting
	if (p.lighting_enabled)
		glEnable(GL_LIGHTING);
	else
		glDisable(GL_LIGHTING);


	if (p.CDM_enabled)
		glEnable(GL_COLOR_MATERIAL);
	else
		glDisable(GL_COLOR_MATERIAL);


	for (unsigned int i = 0; i < p.maxLights; i++)
	{
		//Lighting
		glLightfv(GL_LIGHT0 +i, GL_AMBIENT, p.light_ambient[i]);
		glLightfv(GL_LIGHT0 +i, GL_DIFFUSE, p.light_diffuse[i]);
		glLightfv(GL_LIGHT0 +i, GL_SPECULAR, p.light_specular[i]);
		glLightfv(GL_LIGHT0 +i, GL_POSITION, p.light_position[i]);

		glLightf(GL_LIGHT0 +i, GL_CONSTANT_ATTENUATION, p.light_att[i][0]);
		glLightf(GL_LIGHT0 +i, GL_LINEAR_ATTENUATION, p.light_att[i][1]);
		glLightf(GL_LIGHT0 +i, GL_QUADRATIC_ATTENUATION, p.light_att[i][2]);

		if (p.light_enabled[i])
			glEnable(GL_LIGHT0 +i);
		else
			glDisable(GL_LIGHT0 +i);
	}


/*

	float x = p.light_position[0][0];
	float y = p.light_position[0][1];
	float z = p.light_position[0][2];
	float radius = 1;
	float subdiv = 6;
	glBegin(GL_TRIANGLE_FAN);
    glNormal3f(0, 0, 0);
    glColor4f(1, 1, 1, 1);
    glVertex3f(x, y, z);
    for( int i=0; i<=subdiv; ++i )
    {
        glColor4f(1, 1, 1, 1);
		glVertex3f(x+radius*(float)cos(FLOAT_2PI*(float)i/subdiv), x+radius*(float)sin(FLOAT_2PI*(float)i/subdiv), z);
    }
	glEnd();

*/

	glMaterialfv(GL_FRONT, GL_SPECULAR, p.mat_specular);
	glMaterialfv(GL_FRONT, GL_AMBIENT, p.mat_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, p.mat_diffuse);
    glMaterialfv(GL_FRONT, GL_SHININESS, &p.mat_shininess);

    // Actual drawing
    // Draw green surface
	glPushMatrix();
    glCallList(OBJECT_GROUND_SURFACE);

    p.angle += p.angular_vel * time_delta;
    if (p.angle > 360.0)
        p.angle -= 360;

    glRotatef(p.angle, 0.25f, 1.0f, 0.75f);

    // Draw rotating triangles
    glCallList(OBJECT_ROTATING_TRIANGLES);
	glPopMatrix();

	DrawLightStars();
}


void DrawDebugText( )
{
    CGFXParam& p = GetGFXParam();
    char    text[50];


            sprintf(text, "Mouse X = %d", p.mouse_x);
            DrawString( 10, 40, text );

            sprintf(text, "Mouse Y = %d", p.mouse_y);
            DrawString( 10, 60, text );

            sprintf(text, "Cam X = %f", p.cam_p.x);
            DrawString( 10, 80, text );
            sprintf(text, "Cam Y = %f", p.cam_p.y);
            DrawString( 10, 100, text );
            sprintf(text, "Cam Z = %f", p.cam_p.z);
            DrawString( 10, 120, text );

            sprintf(text, "Cam orient X = %f", p.cam_o.x);
            DrawString( 10, 140, text );
            sprintf(text, "Cam orient Y = %f", p.cam_o.y);
            DrawString( 10, 160, text );
            sprintf(text, "Cam orient Z = %f", p.cam_o.z);
            DrawString( 10, 180, text );

            sprintf(text, "fi = %f", p.fi);
            DrawString( 10, 200, text );
            sprintf(text, "theta = %f", p.theta);
            DrawString( 10, 220, text );
}


/*******************************************************************************
*******************************************************************************
**	Create OpenGL command lists to draw objects later on.
**
**	@param  TODO  TODO
**
*******************************************************************************/
void printString(char *s)
{
    glPushAttrib (GL_LIST_BIT);
    glListBase(fontOffset);
    glCallLists(strlen(s), GL_UNSIGNED_BYTE, (GLubyte *) s);
    glPopAttrib ();
}


void DrawString( int x, int y, char* text )
{
    CGFXParam& p = GetGFXParam();

    glPushMatrix();
    glPushAttrib( GL_ALL_ATTRIB_BITS );

	glDisable(GL_LIGHTING);

    // Print srings
    //glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();
    glColor3f(1.0, 1.0, 1.0);

    double xo,yo,zo;
    WinPos2XYZ( p.fovy, p.nearv, (double) p.width, (double) p.height, x, y, xo, yo, zo );

    glRasterPos3f(xo, yo, zo);
    printString(text);

    glPopAttrib();
    glPopMatrix();
}


/*******************************************************************************
*******************************************************************************
**	Create OpenGL command lists to draw objects later on.
**
**	@param  TODO  TODO
**
*******************************************************************************/
void CreateObjectList()
{
	glm::vec3 A = glm::vec3(5.0f, -3.0f, 5.0f);
	glm::vec3 B = glm::vec3(5.0f, -3.0f, -5.0f);
	glm::vec3 C = glm::vec3(-5.0f, -3.0f, -5.0f);
	glm::vec3 normal1 = computeNormal(C, B, A);

	glm::vec3 D = glm::vec3(5.0f, -3.0f, 5.0f);
	glm::vec3 E = glm::vec3(-5.0f, -3.0f, -5.0f);
	glm::vec3 F = glm::vec3(-5.0f, -3.0f, 5.0f);
	glm::vec3 normal2 = computeNormal(F, E, D);

    glNewList( OBJECT_GROUND_SURFACE, GL_COMPILE );
    glPushMatrix();
    glPushAttrib( GL_CURRENT_BIT );

//        glLoadIdentity();
    glBegin( GL_TRIANGLES );
    glColor3f(0.0f, 1.0f, 0.0f );

	glNormal3fv(glm::value_ptr(normal1));
    glVertex3fv(glm::value_ptr(A));
    glVertex3fv(glm::value_ptr(B));
    glVertex3fv(glm::value_ptr(C));

	glNormal3fv(glm::value_ptr(normal2));
    glVertex3fv(glm::value_ptr(D));
    glVertex3fv(glm::value_ptr(E));
    glVertex3fv(glm::value_ptr(F));
    glEnd();

    glPopAttrib();
    glPopMatrix();
    glEndList();


////////////////////////////
	glm::vec3 G = glm::vec3(0.0f, 3.0f, -4.0f);
	glm::vec3 H = glm::vec3(3.0f, -2.0f, -4.0f);
	glm::vec3 I = glm::vec3(-3.0f, -2.0f, -4.0f);
	glm::vec3 normal3 = computeNormal(G, H, I);

	glm::vec3 J = glm::vec3(0.0f, 3.0f, -3.0);
	glm::vec3 K = glm::vec3(3.0f, -2.0f, -2.0f);
	glm::vec3 L = glm::vec3(-3.0f, -2.0f, 2.0f);
	glm::vec3 normal4 = computeNormal(J, K, L);


    glNewList( OBJECT_ROTATING_TRIANGLES, GL_COMPILE );
    glPushMatrix();
    glPushAttrib( GL_CURRENT_BIT );

    glBegin( GL_TRIANGLES );
	glNormal3fv(glm::value_ptr(normal3));
    glColor3f(0.1f, 0.0f, 0.0f );
    glVertex3fv(glm::value_ptr(G));
    glColor3f(0.0f, 1.0f, 0.0f );
    glVertex3fv(glm::value_ptr(H));
    glColor3f(0.0f, 0.0f, 1.0f );
    glVertex3fv(glm::value_ptr(I));
    glEnd();

    glBegin( GL_TRIANGLES );
	glNormal3fv(glm::value_ptr(normal4));
    glColor3f(0.0f, 0.1f, 0.0f );
    glVertex3fv(glm::value_ptr(J));
    glColor3f(0.0f, 0.0f, 1.0f );
    glVertex3fv(glm::value_ptr(K));
    glColor3f(1.0f, 0.0f, 0.0f );
    glVertex3fv(glm::value_ptr(L));
    glEnd();


    glPopAttrib();
    glPopMatrix();
    glEndList();


////////////////////////////
/*
	glNewList(OBJECT_HALO, GL_COMPILE);
    DrawSubdivHaloZ(0, 0, 0, 1, 32);
    glEndList();
*/

////////////////////////////
    makeRasterFont();
}
