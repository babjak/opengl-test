#include "flags.h"


/*******************************************************************************
*******************************************************************************
**	Set a given bit in a flag.
**
**	@param  TODO  TODO
**
*******************************************************************************/
void SetFlagVal( flags_t& f, flags_t b, unsigned char Value )
{
    Value ? f = (f & ~b) | b : f &= ~b;
}

void SetFlag( flags_t& f, flags_t b )
{
    f = (f & ~b) | b;
}

void ResetFlag( flags_t& f, flags_t b )
{
    f &= ~b;
}

void FlipFlag( flags_t& f, flags_t b )
{
    SetFlagVal( f, b, !GetFlag( f, b ) );
}


/*******************************************************************************
*******************************************************************************
**	Read a given bit from flag.
**
**	@param  TODO  TODO
**
*******************************************************************************/
char GetFlag( flags_t& f, flags_t b )
{
    return f & b;
}
