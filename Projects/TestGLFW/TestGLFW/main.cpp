#include "main.h"

CParam p;

CWHParam& GetWHParam()
{
    return p;
}


CGFXParam& GetGFXParam()
{
    return p;
}


CGUIParam& GetGUIParam()
{
    return p;
}


/*******************************************************************************
*******************************************************************************
**	Calculate time.
**
**	@param  TODO  TODO
**
*******************************************************************************/
void CalculateTime( )
{
    double time_now = glfwGetTime();

    if ( time_now > TIME_RESET_UNIT )
        glfwSetTime( 0.0 );


    p.time_delta = time_now - p.time_previous;

    p.time_previous = time_now > TIME_RESET_UNIT ? 0 : time_now;
}


/*******************************************************************************
*******************************************************************************
**	Measure FPS.
**
**	@param  TODO  TODO
**
*******************************************************************************/
void MeasureFPS( )
{
    p.FPS_meas_time += p.time_delta;
    if ( p.FPS_meas_time > FPS_MEASUREMENT_UNIT )
    {
        p.FPS = ((double) p.frame_counter) / p.FPS_meas_time;
        p.frame_counter = 0;
        p.FPS_meas_time = 0.0;
    }

    p.frame_counter++;
}


/*******************************************************************************
*******************************************************************************
**	Check exit condition for main loop.
**
**	@param  TODO  TODO
**
*******************************************************************************/
char ExitCondition()
{
    if ( glfwGetWindowParam( GLFW_OPENED ) == GL_FALSE )
        SetFlag( p.status_flags, STATUS_EXIT );

    if ( GetFlag( p.status_flags, STATUS_EXIT ) )
        return 1;

    if ( GetFlag( p.status_flags, STATUS_FS_WIN_CHANGE_REQ ) )
    {
        // flip full screen flag
        FlipFlag( p.status_flags, STATUS_FULL_SCREEN );
        return 1;
    }

    return 0;
}


/*******************************************************************************
*******************************************************************************
**	The main function. The.
**
**	@param  TODO  TODO
**
*******************************************************************************/
int main()
{
    char    text[50];

/////////////////////////////////////////////////
// Configuration initialization
    // Show mouse cursor
    p.state = STATE_GOD_MODE_WITH_MOUSE;

/////////////////////////////////////////////////
//  The meat

    // Init
    if ( InitGL() == GL_FALSE )
        return EXIT_FAILURE;

    do
    {
        // Opening window/screen
        if ( !OpenWindow() )
        {
            //TODO: probably that's not the proper way to exit...
            glfwTerminate();
            return EXIT_FAILURE;
        }

		InitGUI();

        // Init other stuff...
        InitMisc();

		InitGFX();

        // Create GL lists to draw objects
        CreateObjectList();

		CreateGUI(); // Number of lights gets set in Init!!!! Init has to come first

        if ( GetFlag( p.status_flags, STATUS_MAIN_MENU ) )
            ShowExitDialog( 1 );
        else
            ShowExitDialog( 0 );

        do
        {
            CalculateTime();

            MeasureFPS();

            Draw( p.time_delta );

            sprintf(text, "%d FPS", (int) p.FPS);
            DrawString( 10, 20, text );

            //DrawDebugText( );

			int x_coord = p.width/2-30;
            if ( p.state == STATE_PLAY )
				DrawString( x_coord, 20, "Play mode" );
            else if ( p.state == STATE_GOD_MODE )
                DrawString( x_coord, 20, "God mode" );
            else if ( p.state == STATE_GOD_MODE_WITH_MOUSE )
                DrawString( x_coord, 20, "God mode (mouse)" );

            if ( GetFlag( p.status_flags, STATUS_MAIN_MENU ) )
                DrawString( x_coord, 40, "Main menu" );

//TODO: Why does this help removing the error messages from the background?
// Because I had a popattributes instead of a pushattributes in the string drawing function... DUUUHHHHH
/*			GLenum	the_error;
			the_error = glGetError();*/

#ifdef USE_ANTTWEAKBAR
			// Draw tweak bars
			TwDraw();
#endif

#ifdef USE_GLGOOEY
			// this must be called once per frame
			WindowManager::instance().update();
#endif

            // Swap buffers
            glFlush();
            glfwSwapBuffers();

        } while ( !ExitCondition() );

#ifdef USE_GLGOOEY

		WindowManager::instance().removeWindow(&(p.console));
		WindowManager::instance().terminate();
#endif

#ifdef USE_ANTTWEAKBAR
		TwTerminate();
//		delete p.bar; //<- Makes program hang WTF?! TODO
//		free(p.bar);  //<- Makes program hang WTF?! TODO
#endif

		glfwCloseWindow();
    } while ( !GetFlag( p.status_flags, STATUS_EXIT ) );

    glfwTerminate();
    return EXIT_SUCCESS;
}
