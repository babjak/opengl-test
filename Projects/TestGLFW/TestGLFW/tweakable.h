#ifndef TWEAKABLE_H_INCLUDED
#define TWEAKABLE_H_INCLUDED

#include <GL/glfw.h>

typedef GLfloat GLfloat4[4]; 
typedef GLfloat GLfloat3[3]; 

/////////////////////////////////////////////////
//
// Parameter structure
//
/////////////////////////////////////////////////
class CTweakable
{
    public:
        double          angular_vel;    // [degrees]/[seconds]

		char			lighting_enabled;
		char			CDM_enabled;
		
//light
		GLint			maxLights;

		GLfloat			mat_specular[4];
		GLfloat			mat_ambient[4];
		GLfloat			mat_diffuse[4];
		GLfloat			mat_shininess;

		char			*light_enabled;
		GLfloat4		*light_specular;
		GLfloat4		*light_ambient;
		GLfloat4		*light_diffuse;
		GLfloat4		*light_position;
		GLfloat3		*light_att;

//Coordinate system
		char			Axis_enabled;
		float			Axis_length;
		unsigned char	Axis_width;

    CTweakable()
        :
		angular_vel(360.0/10.0),
		maxLights(0),
		lighting_enabled(1),
		CDM_enabled(0),
		Axis_enabled(0),
		Axis_length(500.0),
		Axis_width(1)
	{
		mat_specular[0] = 1;
		mat_specular[1] = 1;
		mat_specular[2] = 1;
		mat_specular[3] = 1;

		mat_ambient[0] = 1;
		mat_ambient[1] = 1;
		mat_ambient[2] = 1;
		mat_ambient[3] = 1;

		mat_diffuse[0] = 1;
		mat_diffuse[1] = 1;
		mat_diffuse[2] = 1;
		mat_diffuse[3] = 1;
    }

	bool CTweakable::operator==(const CTweakable &other) const 
	{
		return 
			other.mat_specular[0] == this->mat_specular[0]
			&&
			other.mat_specular[1] == this->mat_specular[1]
			&&
			other.mat_specular[2] == this->mat_specular[2]
			&&
			other.mat_specular[3] == this->mat_specular[3]
			&&
			other.mat_ambient[0] == this->mat_ambient[0]
			&&
			other.mat_ambient[1] == this->mat_ambient[1]
			&&
			other.mat_ambient[2] == this->mat_ambient[2]
			&&
			other.mat_ambient[3] == this->mat_ambient[3]
			&&
			other.mat_diffuse[0] == this->mat_diffuse[0]
			&&
			other.mat_diffuse[1] == this->mat_diffuse[1]
			&&
			other.mat_diffuse[2] == this->mat_diffuse[2]
			&&
			other.mat_diffuse[3] == this->mat_diffuse[3]
			&&
			other.mat_shininess == this->mat_shininess;
	}

	bool CTweakable::operator!=(const CTweakable &other) const 
	{
		return !(*this == other);
	}

};

#endif // TWEAKABLE_H_INCLUDED
