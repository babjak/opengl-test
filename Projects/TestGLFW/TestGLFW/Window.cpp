#include "Window.h"

/////////////////////////////////////////////////
//
// Value declaration
//
/////////////////////////////////////////////////
#define VIDMODE_LIST_MAXCOUNT       40


/*******************************************************************************
*******************************************************************************
**	Init OpenGL and take care of some stuff.
**
**	@param  TODO  TODO
**
*******************************************************************************/
void SetMouseVisibilityRelatedStuff( )
{
    CWHParam& p = GetWHParam();

    if ( p.state == STATE_GOD_MODE_WITH_MOUSE || GetFlag( p.status_flags, STATUS_MAIN_MENU ) )
    {
        glfwEnable( GLFW_MOUSE_CURSOR );

		SetMouseCallbacks(1);

        p.mouse_x = p.mouse_x_previous;
        p.mouse_y = p.mouse_y_previous;
    }
    else
    {
        glfwDisable( GLFW_MOUSE_CURSOR );

		SetMouseCallbacks(0);

		p.mouse_x_previous = p.width/2;
        p.mouse_y_previous = p.height/2;
    }
}




/*******************************************************************************
*******************************************************************************
**	Init OpenGL and take care of some stuff.
**
**	@param  TODO  TODO
**
*******************************************************************************/
int InitGL( )
{
    CWHParam& p = GetWHParam();

    if ( glfwInit() == GL_FALSE )
        return GL_FALSE;

    // Get a list of video modes
    int             vidmode_list_count;
    GLFWvidmode     vidmode_list[VIDMODE_LIST_MAXCOUNT];
    vidmode_list_count = glfwGetVideoModes( vidmode_list, VIDMODE_LIST_MAXCOUNT );

    glfwGetDesktopMode( &(p.current_vidmode) );

    return GL_TRUE;
}


/*******************************************************************************
*******************************************************************************
**	Open a window for OpenGL to draw in.
**
**	@param  TODO  TODO
**
*******************************************************************************/
int OpenWindow( )
{
    CWHParam& p = GetWHParam();

    int retval;
    if ( GetFlag( p.status_flags, STATUS_FULL_SCREEN ) )
    {
        retval = glfwOpenWindow( p.current_vidmode.Width,      //width
                                 p.current_vidmode.Height,     //height
                                 p.current_vidmode.RedBits,    //redbits
                                 p.current_vidmode.GreenBits,  //greenbits
                                 p.current_vidmode.BlueBits,   //bluebits
                                 16,                            //alphabits
                                 16,                            //depthbits
                                 16,                            //stencilbits
                                 GLFW_FULLSCREEN );            //mode
        if ( !retval  )
            return retval;
    }
    else
    {
        retval = glfwOpenWindow( p.win_width,   //width
                                 p.win_height,  //height
                                 16,             //redbits
                                 16,             //greenbits
                                 16,             //bluebits
                                 16,             //alphabits
                                 16,             //depthbits
                                 16,             //stencilbits
                                 GLFW_WINDOW ); //mode

        if ( !retval  )
            return retval;
    }


    //TODO: WHAT'S THIS?
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE);


    return 1;
}


/*******************************************************************************
*******************************************************************************
**	Miscellaneous initialization stuff...
**
**	@param  TODO  TODO
**
*******************************************************************************/
void InitMisc()
{
    CWHParam& p = GetWHParam();

    ResetFlag( p.status_flags, STATUS_FS_WIN_CHANGE_REQ );

	InitCallback( );
 
	SetMouseVisibilityRelatedStuff( );

	// Keyboard
    glfwEnable( GLFW_STICKY_KEYS );
    glfwEnable( GLFW_KEY_REPEAT );

    // Mouse buttons
    glfwEnable( GLFW_STICKY_MOUSE_BUTTONS );

    glfwSetWindowTitle( "GLFW Application" );
    glfwSwapInterval( 1 );

    p.mouse_x_previous = p.mouse_x;
    p.mouse_y_previous = p.mouse_y;
}
