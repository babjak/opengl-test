#include "Window.h"

#define GLUT_KEY_F1				1
#define GLUT_KEY_F2				2
#define GLUT_KEY_F3				3
#define GLUT_KEY_F4				4
#define GLUT_KEY_F5				5
#define GLUT_KEY_F6				6
#define GLUT_KEY_F7				7
#define GLUT_KEY_F8				8
#define GLUT_KEY_F9				9
#define GLUT_KEY_F10			10
#define GLUT_KEY_F11			11
#define GLUT_KEY_F12			12
/* directional keys */
#define GLUT_KEY_LEFT			100
#define GLUT_KEY_UP				101
#define GLUT_KEY_RIGHT			102
#define GLUT_KEY_DOWN			103
#define GLUT_KEY_PAGE_UP		104
#define GLUT_KEY_PAGE_DOWN		105
#define GLUT_KEY_HOME			106
#define GLUT_KEY_END			107
#define GLUT_KEY_INSERT			108



char ShiftPressed = 0;
char AltPressed = 0;
char CtrlPressed = 0;

void SetMouseCallbacks(char GUI)
{
	if (GUI)
	{
        glfwSetMousePosCallback( mousepos_cbGUIfun );
	    glfwSetMouseButtonCallback( mousebutton_cbGUIfun );
		glfwSetMouseWheelCallback( mousewheel_cbfun );
	}
	else
	{
		glfwSetMousePosCallback( mousepos_cbfun );
	    glfwSetMouseButtonCallback( mousebutton_cbGUIfun );
		glfwSetMouseWheelCallback( mousewheel_cbfun );
	}
}

void InitCallback( )
{
// Window resizing
    glfwSetWindowSizeCallback( windowsize_cbfun );

// Keyboard
    glfwSetKeyCallback( key_cbfun );

	glfwSetCharCallback( char_cbfun );
}


/*******************************************************************************
*******************************************************************************
**	Callback function for window resizing.
**
**	@param  TODO  TODO
**
*******************************************************************************/
void GLFWCALL windowsize_cbfun( int width, int height )
{
    height = height > 0 ? height : 1;
    glViewport( 0, 0, width, height );

    glMatrixMode( GL_PROJECTION );

    glLoadIdentity();


    CWHParam& p = GetWHParam();

    p.width = width;
    p.height = height;

    gluPerspective( p.fovy,
                    (GLfloat)width/(GLfloat)height,
                    p.nearv,
                    100.0 );

    if ( !GetFlag( p.status_flags, STATUS_FULL_SCREEN ) )
    {
        p.win_width = width;
        p.win_height = height;
    }

#ifdef USE_GLGOOEY
    WindowManager::instance().applicationResized(width, height);
#endif

#ifdef USE_ANTTWEAKBAR
    // Send the new window size to AntTweakBar
    TwWindowSize(width, height);
#endif

	ResizeWindowGUI	( width, height );
}

#ifdef USE_GLGOOEY
// **************************************************************************************************
//  In order to let GLGooey know whether one of the modifier keys have been pressed, we must define
//  three functions and pass them to the window manager on initialization. These are the three
//  functions - they simply return true iff the corresponding key is pressed
// **************************************************************************************************
bool isShiftPressed()
{
	return ShiftPressed ? true : false;
}

bool isAltPressed()
{
	return AltPressed ? true : false;
}

bool isCtrlPressed()
{
	return CtrlPressed ? true : false;
}

int GLFW2GLUTkey(int key_id)
{
    switch ( key_id )
    {
        case GLFW_KEY_F1:
			return GLUT_KEY_F1;
            break;

        case GLFW_KEY_F2:
			return GLUT_KEY_F2;
            break;

		case GLFW_KEY_F3:
			return GLUT_KEY_F3;
            break;

        case GLFW_KEY_F4:
			return GLUT_KEY_F4;
            break;

        case GLFW_KEY_F5:
			return GLUT_KEY_F5;
            break;

		case GLFW_KEY_F6:
			return GLUT_KEY_F6;
            break;

		case GLFW_KEY_F7:
			return GLUT_KEY_F7;
            break;

		case GLFW_KEY_F8:
			return GLUT_KEY_F8;
            break;

		case GLFW_KEY_F9:
			return GLUT_KEY_F9;
            break;

		case GLFW_KEY_F10:
			return GLUT_KEY_F10;
            break;

		case GLFW_KEY_F11:
			return GLUT_KEY_F11;
            break;

		case GLFW_KEY_F12:
			return GLUT_KEY_F12;
            break;

		case GLFW_KEY_UP:
			return GLUT_KEY_UP;
            break;

		case GLFW_KEY_DOWN:
			return GLUT_KEY_DOWN;
            break;

		case GLFW_KEY_LEFT:
			return GLUT_KEY_LEFT;
            break;

		case GLFW_KEY_RIGHT:
			return GLUT_KEY_RIGHT;
            break;

		case GLFW_KEY_PAGEUP:
			return GLUT_KEY_PAGE_UP;
            break;

		case GLFW_KEY_PAGEDOWN:
			return GLUT_KEY_PAGE_DOWN;
            break;

		case GLFW_KEY_HOME:
			return GLUT_KEY_HOME;
            break;

		case GLFW_KEY_END:
			return GLUT_KEY_END;
            break;

		case GLFW_KEY_INSERT:
			return GLUT_KEY_INSERT;
            break;

		case GLFW_KEY_SPACE:
			return ' ';
            break;

		case GLFW_KEY_ESC:
			return 27;
            break;

		case GLFW_KEY_TAB:
			return '\t';
            break;

		case GLFW_KEY_ENTER:
			return '\n';
            break;

		case GLFW_KEY_BACKSPACE:
			return '\b';
            break;

		case GLFW_KEY_DEL:
			return 127;
            break;

		default:
			if ( 
				( key_id >= 'A' && key_id <= 'Z' )
				&&
				( !isShiftPressed() ) 
				)
					return key_id + 'a' - 'A';
			else
				return key_id;
            break;
    }

/*
GLFW_KEY_LSUPER
GLFW_KEY_RSUPER
GLFW_KEY_KP_n
GLFW_KEY_KP_DIVIDE
GLFW_KEY_KP_MULTIPLY
GLFW_KEY_KP_SUBTRACT
GLFW_KEY_KP_ADD
GLFW_KEY_KP_DECIMAL
GLFW_KEY_KP_EQUAL
GLFW_KEY_KP_ENTER
GLFW_KEY_KP_NUM_LOCK
GLFW_KEY_CAPS_LOCK
GLFW_KEY_SCROLL_LOCK
GLFW_KEY_PAUSE
GLFW_KEY_MENU
*/

}


unsigned char isSpecialized(int key)
{
	switch ( key )
    {
        case GLFW_KEY_F1:
        case GLFW_KEY_F2:
		case GLFW_KEY_F3:
        case GLFW_KEY_F4:
        case GLFW_KEY_F5:
		case GLFW_KEY_F6:
		case GLFW_KEY_F7:
		case GLFW_KEY_F8:
		case GLFW_KEY_F9:
		case GLFW_KEY_F10:
		case GLFW_KEY_F11:
		case GLFW_KEY_F12:
		case GLFW_KEY_UP:
		case GLFW_KEY_DOWN:
		case GLFW_KEY_LEFT:
		case GLFW_KEY_RIGHT:
		case GLFW_KEY_PAGEUP:
		case GLFW_KEY_PAGEDOWN:
		case GLFW_KEY_HOME:
		case GLFW_KEY_END:
		case GLFW_KEY_INSERT:
			return 1;

		default:
            return 0;
    }
}

#endif

void EnterPlayMode()
{
    CWHParam& p = GetWHParam();

	p.state = STATE_PLAY;

	ShowParamBars(0);
}

void EnterGodMode()
{
    CWHParam& p = GetWHParam();

	p.state = STATE_GOD_MODE_WITH_MOUSE;

	ShowParamBars(1);
}

/*******************************************************************************
*******************************************************************************
**	Callback function for keyboard input.
**
**	@param  TODO  TODO
**
*******************************************************************************/
void GLFWCALL key_cbfun( int key_id, int key_state )
{
#ifdef USE_GLGOOEY
    if ( key_state != GLFW_PRESS )
	{
		if ( !isSpecialized(key_id) )
			WindowManager::instance().onChar( GLFW2GLUTkey(key_id) );

		if ( isSpecialized(key_id) )
			WindowManager::instance().onKeyDown( GLFW2GLUTkey(key_id) );
	}

    switch ( key_id )
    {
        case GLFW_KEY_LSHIFT:
        case GLFW_KEY_RSHIFT:
			ShiftPressed = (key_state == GLFW_PRESS);

            break;

        case GLFW_KEY_LALT:
        case GLFW_KEY_RALT:
			AltPressed = (key_state == GLFW_PRESS);

			break;

        case GLFW_KEY_LCTRL:
        case GLFW_KEY_RCTRL:
			CtrlPressed = (key_state == GLFW_PRESS);

			break;

        default:
            break;
    }
#endif

#ifdef USE_ANTTWEAKBAR
    // - Directly redirect GLFW key events to AntTweakBar
    TwEventKeyGLFW(key_id, key_state);
#endif

    CWHParam& p = GetWHParam();

    if ( key_state == GLFW_PRESS )
    {
        if ( GetFlag( p.status_flags, STATUS_MAIN_MENU ) )
        {
        }
        else
        {
            if ( p.state == STATE_PLAY || p.state == STATE_GOD_MODE )
            {
                switch ( key_id )
                {
                case 'w':
                case 'W':
                case GLFW_KEY_UP:
                    SetFlag( p.movement_flags, UP_BIT );
                    break;

                case 's':
                case 'S':
                case GLFW_KEY_DOWN:
                    SetFlag( p.movement_flags, DOWN_BIT );
                    break;

                case 'a':
                case 'A':
                case GLFW_KEY_LEFT:
                    SetFlag( p.movement_flags, LEFT_BIT );
                    break;

                case 'd':
                case 'D':
                case GLFW_KEY_RIGHT:
                    SetFlag( p.movement_flags, RIGHT_BIT );
                    break;

                default:
                    break;
                }
            }
        }
    }
    else
    {
        if ( GetFlag( p.status_flags, STATUS_MAIN_MENU ) )
        {
            switch ( key_id )
            {
            case GLFW_KEY_ESC:
                SetFlag( p.status_flags, STATUS_EXIT );
                break;

            case 'r':
            case 'R':
                ResetFlag( p.status_flags, STATUS_MAIN_MENU );
				ShowExitDialog( 0 );

				SetMouseVisibilityRelatedStuff( );
                break;

			default:
                break;
            }
        }
        else
        {
            if ( p.state == STATE_PLAY || p.state == STATE_GOD_MODE )
            {
                switch ( key_id )
                {
                case 'w':
                case 'W':
                case GLFW_KEY_UP:
                    ResetFlag( p.movement_flags, UP_BIT );
                    break;

                case 's':
                case 'S':
                case GLFW_KEY_DOWN:
                    ResetFlag( p.movement_flags, DOWN_BIT );
                    break;

                case 'a':
                case 'A':
                case GLFW_KEY_LEFT:
                    ResetFlag( p.movement_flags, LEFT_BIT );
                    break;

                case 'd':
                case 'D':
                case GLFW_KEY_RIGHT:
                    ResetFlag( p.movement_flags, RIGHT_BIT );
                    break;

                default:
                    break;
                }
            }

            if ( p.state == STATE_GOD_MODE || p.state == STATE_GOD_MODE_WITH_MOUSE )
            {
                switch ( key_id )
                {
                case 'm':
                case 'M':
                    if ( p.state == STATE_GOD_MODE )
                        p.state = STATE_GOD_MODE_WITH_MOUSE;
                    else
                        p.state = STATE_GOD_MODE;

                    SetMouseVisibilityRelatedStuff( );
                    break;

                default:
                    break;
                }
            }


            switch ( key_id )
            {
            case 'g':
            case 'G':
                if ( p.state == STATE_GOD_MODE )
					EnterPlayMode();
//                    p.state = STATE_PLAY;
				else if ( p.state == STATE_GOD_MODE_WITH_MOUSE )
				{
					EnterPlayMode();
//                    p.state = STATE_PLAY;
					SetMouseVisibilityRelatedStuff( );
				}
                else
                {
					EnterGodMode();
//                    p.state = STATE_GOD_MODE_WITH_MOUSE;
                    p.movement_flags = 0;
	                SetMouseVisibilityRelatedStuff( );
                }


//                if ( p.state == STATE_GOD_MODE || p.state == STATE_GOD_MODE_WITH_MOUSE )
//                    CreateParamBar( );
//                else
//                    DestroyParamBar( );

                break;

            case GLFW_KEY_ESC:
                SetFlag( p.status_flags, STATUS_MAIN_MENU );
				ShowExitDialog( 1 );

                p.movement_flags = 0;
                SetMouseVisibilityRelatedStuff( );

                break;

            default:
                break;
            }
        }

        switch ( key_id )
        {
        case 'f':
        case 'F':
            SetFlag( p.status_flags, STATUS_FS_WIN_CHANGE_REQ );
            break;

        default:
            break;
        }
    }
}


/*******************************************************************************
*******************************************************************************
**	Callback function for mouse wheel input.
**
**	@param  TODO  TODO
**
*******************************************************************************/
void GLFWCALL char_cbfun( int char_id, int key_state )
{
#ifdef USE_ANTTWEAKBAR
	TwEventCharGLFW(char_id, key_state);
#endif
}


/*******************************************************************************
*******************************************************************************
**	Callback function for mouse movement.
**
**	@param  TODO  TODO
**
*******************************************************************************/
void GLFWCALL mousepos_cbGUIfun( int x, int y )
{
#ifdef USE_GLGOOEY
    WindowManager::instance().onMouseMove(x, y);
#endif

#ifdef USE_ANTTWEAKBAR
	TwEventMousePosGLFW(x, y);
#endif
}


/*******************************************************************************
*******************************************************************************
**	Callback function for mouse movement.
**
**	@param  TODO  TODO
**
*******************************************************************************/
void GLFWCALL mousepos_cbfun( int x, int y )
{
#ifdef USE_GLGOOEY
    WindowManager::instance().onMouseMove(x, y);
#endif

#ifdef USE_ANTTWEAKBAR
	TwEventMousePosGLFW(x, y);
#endif

    CWHParam& p = GetWHParam();

//    if (!GetFlag(p.status_flags, STATUS_MOUSE_VISIBLE_BIT))
//    {
    p.mouse_x = x;
    p.mouse_y = y;
//    }
}


/*******************************************************************************
*******************************************************************************
**	Callback function for mouse button input.
**
**	@param  TODO  TODO
**
*******************************************************************************/
void GLFWCALL mousebutton_cbGUIfun( int button_id, int button_state )
{
//	CWHParam& p = GetWHParam();
	int x, y;

	glfwGetMousePos( &x, &y );

#ifdef USE_GLGOOEY
		switch ( button_id )
		{
			case GLFW_MOUSE_BUTTON_LEFT:
				if ( button_state == GLFW_PRESS )
					WindowManager::instance().onLeftButtonDown(x, y);
				else
					WindowManager::instance().onLeftButtonUp(x, y);

				break;
			case GLFW_MOUSE_BUTTON_MIDDLE:
				if ( button_state == GLFW_PRESS )
					WindowManager::instance().onMiddleButtonDown(x, y);
				else				
					WindowManager::instance().onMiddleButtonUp(x, y);

				break;
			case GLFW_MOUSE_BUTTON_RIGHT:
				if ( button_state == GLFW_PRESS )
					WindowManager::instance().onRightButtonDown(x, y);
				else
					WindowManager::instance().onRightButtonUp(x, y);
				
				break;
			default:
				break;
		}
#endif

#ifdef USE_ANTTWEAKBAR
	TwEventMouseButtonGLFW( button_id, button_state );
#endif
}


/*******************************************************************************
*******************************************************************************
**	Callback function for mouse button input.
**
**	@param  TODO  TODO
**
*******************************************************************************/
void GLFWCALL mousebutton_cbfun( int button_id, int button_state )
{
	if (button_state != GLFW_PRESS)
        return;

    switch ( button_id )
    {
    case GLFW_MOUSE_BUTTON_LEFT:
        break;
    case GLFW_MOUSE_BUTTON_MIDDLE:
        break;
    case GLFW_MOUSE_BUTTON_RIGHT:
        break;
    default:
        break;
    }
}


/*******************************************************************************
*******************************************************************************
**	Callback function for mouse wheel input.
**
**	@param  TODO  TODO
**
*******************************************************************************/
void GLFWCALL mousewheel_cbfun( int wheel_pos )
{
#ifdef USE_ANTTWEAKBAR
    TwEventMouseWheelGLFW( wheel_pos );
#endif
}
