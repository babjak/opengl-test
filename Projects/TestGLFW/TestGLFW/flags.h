#ifndef FLAGS_H_INCLUDED
#define FLAGS_H_INCLUDED


/////////////////////////////////////////////////
//
// Value declaration
//
/////////////////////////////////////////////////
#define UP_BIT      1
#define DOWN_BIT    2
#define LEFT_BIT    4
#define RIGHT_BIT   8

#define STATE_PLAY                  0
#define STATE_GOD_MODE              1
#define STATE_GOD_MODE_WITH_MOUSE   2
//#define STATE_MAIN_MENU_PLAY        3
//#define STATE_MAIN_MENU_GOD_MODE    4

#define STATUS_EXIT                 1
#define STATUS_FULL_SCREEN          4
#define STATUS_FS_WIN_CHANGE_REQ    8
#define STATUS_GOD_MODE             16
#define STATUS_MAIN_MENU            32


typedef     unsigned char       flags_t;


/////////////////////////////////////////////////
//
// Parameter structure
//
/////////////////////////////////////////////////
class CFlags
{
    public:
        flags_t         movement_flags;
        flags_t         status_flags;
        unsigned char   state;

    CFlags()
        :
        movement_flags(0),
        status_flags(0),
        state(STATE_GOD_MODE_WITH_MOUSE)
    {

    }

	bool CFlags::operator==(const CFlags &other) const 
	{
		return 
			other.movement_flags == this->movement_flags 
			&&
			other.state == this->state
			&&
			other.status_flags == this->status_flags;
	}

	bool CFlags::operator!=(const CFlags &other) const 
	{
		return !(*this == other);
	}
};


/////////////////////////////////////////////////
//
// Functions implemented in .cpp
//
/////////////////////////////////////////////////
void        SetFlag     ( flags_t& f, flags_t b );
void        ResetFlag   ( flags_t& f, flags_t b );
char        GetFlag     ( flags_t& f, flags_t b );
void        FlipFlag    ( flags_t& f, flags_t b );

#endif // FLAGS_H_INCLUDED
