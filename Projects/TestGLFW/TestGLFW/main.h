#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#include <stdio.h>

#include "flags.h"
#include "Window.h"
#include "gui.h"
#include "gfx.h"



/////////////////////////////////////////////////
//
// Value declaration
//
/////////////////////////////////////////////////
#define TIME_RESET_UNIT             5.0  //in seconds
#define FPS_MEASUREMENT_UNIT        1.5  //in seconds


/////////////////////////////////////////////////
//
// Parameter structure
//
/////////////////////////////////////////////////
class CParam :
    public CGFXParam,
    public CWHParam,
	public CGUIParam
{
    public:
        double          time_now;
        double          time_previous;
        double          time_delta;

        unsigned short  frame_counter;
        double          FPS;
        double          FPS_meas_time;

    CParam()
        :
        time_now(0.0),
        time_previous(0.0),
        time_delta(0.0),

        frame_counter(0),
        FPS(0.0),
        FPS_meas_time(0.0)
    {

    }

	bool CParam::operator==(const CParam &other) const 
	{
		return
			*((CGUIParam*) &other) == *((CGUIParam*) this)
			&&
			*((CGFXParam*) &other) == *((CGFXParam*) this)
			&&	
			*((CWHParam*) &other) == *((CWHParam*) this)
			&&
			other.time_now == this->time_now
			&&
			other.time_previous == this->time_previous
			&&
			other.time_delta == this->time_delta
			&&
			other.frame_counter == this->frame_counter
			&&
			other.FPS == this->FPS
			&&
			other.FPS_meas_time == this->FPS_meas_time;
	}

	bool CParam::operator!=(const CParam &other) const 
	{
		return !(*this == other);
	}

};

#endif // MAIN_H_INCLUDED
