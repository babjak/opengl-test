#include "MathStuff.h"


glm::vec3 computeNormal(glm::vec3 const & a, 
						glm::vec3 const & b,
						glm::vec3 const & c)
{
	return glm::normalize(glm::cross(c - a, b - a));
}


glm::vec3 eucl2polar(glm::vec3 const & a)
{
	glm::vec3 b;
//WARNING! Theta measured from Y, and Phi measured from Z
	b.x = glm::length(a);		//	R
	b.y = glm::acos(a.y / b.x);	//	theta
	b.z = glm::atan(a.x, a.z);	//	phi

	return b;
}


void WinPos2XYZ( double fovy, double n, double w, double h, int x, int y, double& X, double& Y, double& Z )
{
/*
	CGFXParam& p = GetGFXParam();

    double fovy = p.fovy;
    double n    = p.nearv;
    double w    = (double) p.width;
    double h    = (double) p.height;
*/

    double temp = 2/h * n * tan(radians(fovy/2));

    X = (x - w/2) * temp;
    Y = (h/2 - y) * temp;
    Z = -n;
}