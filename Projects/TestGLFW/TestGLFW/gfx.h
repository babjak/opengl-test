#ifndef GFX_H_INCLUDED
#define GFX_H_INCLUDED

#include <stdio.h>
#include <GL/glfw.h>
#include <glm.hpp>
#include <gtc/type_ptr.hpp>
#include <string.h>

#include "WindowParam.h"
#include "flags.h"
#include "tweakable.h"

using namespace glm;


/////////////////////////////////////////////////
//
// Parameter structure
//
/////////////////////////////////////////////////
class CGFXParam :
    public virtual CWinParam,
    public virtual CFlags,
	public virtual CTweakable
{
    public:
//conf
        vec3            cam_p;          // Camera position
        double          cam_speed;      // Camera speed [unit length]/[seconds]

        vec3            cam_o;          // Camera orientation
        double          cam_angular_vel;// Camera angular velocity [degrees]/[pixel]
        double          fi;             // Camera orientation
        double          theta;          // Camera orientation

//misc conf
//        double          angular_vel;    // [degrees]/[seconds]
        double          angle;

//state
        GLuint          fontOffset;

/*
//light
		GLfloat			mat_specular[4];
		GLfloat			mat_ambient[4];
		GLfloat			mat_diffuse[4];
		GLfloat			mat_shininess;
//		GLfloat light_position[] = { 1.0, 1.0, 1.0, 0.0 };
*/


    CGFXParam()
        :
        cam_p(0.0f, 0.0f, 30.0f),
        cam_speed(17.0),
        cam_o(0.0f, 0.0f, -1.0f),
        cam_angular_vel(0.1),
        fi(0.0),
        theta(0.0),
//        angular_vel(360.0/10.0),
        angle(0.0)
    {

    }

	bool CGFXParam::operator==(const CGFXParam &other) const 
	{
		return
			*((CFlags*) &other) == *((CFlags*) this)
			&&
			*((CWinParam*) &other) == *((CWinParam*) this)
			&&
			other.cam_p == this->cam_p
			&&
			other.cam_speed == this->cam_speed
			&&
			other.cam_o == this->cam_o
			&&
			other.cam_angular_vel == this->cam_angular_vel
			&&
			other.fi == this->fi
			&&
			other.theta == this->theta
			&&
			other.angular_vel == this->angular_vel
			&&
			other.angle == this->angle
			&&
			other.fontOffset == this->fontOffset;
	}

	bool CGFXParam::operator!=(const CGFXParam &other) const 
	{
		return !(*this == other);
	}

};


/////////////////////////////////////////////////
//
// Functions implemented in .cpp visible
// from outside
//
/////////////////////////////////////////////////
void	InitGFX				( );
void    CreateObjectList    ( );
void    Draw                ( double time_delta );
void    DrawDebugText       ( );
void    DrawString          ( int x, int y, char* text );


/////////////////////////////////////////////////
//
// Functions needed, but implemented somewhere else...
//
/////////////////////////////////////////////////
CGFXParam& GetGFXParam();

#endif // GFX_H_INCLUDED
