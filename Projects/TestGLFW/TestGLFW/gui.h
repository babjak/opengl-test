#ifndef GUI_H_INCLUDED
#define GUI_H_INCLUDED

#define USE_ANTTWEAKBAR
#ifdef USE_ANTTWEAKBAR
#include <AntTweakBar.h>
#endif

#define USE_GLGOOEY
#ifdef USE_GLGOOEY
#include "glgooey/core/StandardException.h"
#include "glgooey/core/Utils.h"
#include "glgooey/core/Rectangle.h"

#include "glgooey/WindowManager.h"
#include "glgooey/FrameWindow.h"
#include "glgooey/Button.h"
#include "glgooey/Panel.h"
#include "glgooey/EditField.h"
#include "glgooey/ListBox.h"
#include "glgooey/TimeManager.h"
#include "glgooey/PropertyScheme.h"
#include "glgooey/GridLayouter.h"
#endif

#include "flags.h"
#include "tweakable.h"


/////////////////////////////////////////////////
//
// GUI class
//
/////////////////////////////////////////////////
#ifdef USE_GLGOOEY
using namespace Gooey;

class Console 
	: 
	public Panel
{
public:
    Console();
    // this is the slot that responds to the button being pressed
    void ExitButtonPressed();
    void ResumeButtonPressed();

private:
    // the controls
	Button      ExitButton_;
	Button      ResumeButton_;
};
#endif


/////////////////////////////////////////////////
//
// Parameter structure
//
/////////////////////////////////////////////////
class CGUIParam :
	public virtual CFlags,
	public virtual CTweakable
{
    public:
#ifdef USE_GLGOOEY
		Console		console;
#endif
#ifdef USE_ANTTWEAKBAR
		TwBar		*bar;         // Pointer to a tweak bar
		TwBar		*Lightbar;
#endif

    CGUIParam()
    {
    }

	bool CGUIParam::operator==(const CGUIParam &other) const 
	{
		return
			*((CFlags*) &other) == *((CFlags*) this);
	}

	bool CGUIParam::operator!=(const CGUIParam &other) const 
	{
		return !(*this == other);
	}

};


/////////////////////////////////////////////////
//
// Functions implemented in .cpp visible
// from outside
//
/////////////////////////////////////////////////
void	InitGUI			( );
void	CreateGUI		( );
void	ResizeWindowGUI	( int width, int height );
void	ShowExitDialog	( unsigned char show );
void	ShowParamBars	( unsigned char show );


// Callback functions
//extern	void	exit_button_cb ( puObject * );
//extern	void	resume_button_cb ( puObject * );


/////////////////////////////////////////////////
//
// Functions needed, but implemented somewhere else...
//
/////////////////////////////////////////////////
bool		isShiftPressed	();
bool		isAltPressed	();
bool		isCtrlPressed	();

CGUIParam&	GetGUIParam( );
void		SetMouseVisibilityRelatedStuff( );

#endif // FLAGS_H_INCLUDED