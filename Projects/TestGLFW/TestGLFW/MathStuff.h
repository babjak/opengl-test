#ifndef MATHSTUFF_H_INCLUDED
#define MATHSTUFF_H_INCLUDED

#include <glm.hpp>
using namespace glm;

#define MIN(a, b)  (((a) < (b)) ? (a) : (b))
#define MAX(a, b)  (((a) > (b)) ? (a) : (b))

#define FLOAT_2PI					6.283185307 // 2*PI


glm::vec3 computeNormal(glm::vec3 const & a, 
						glm::vec3 const & b,
						glm::vec3 const & c);
glm::vec3 eucl2polar(glm::vec3 const & a);
void WinPos2XYZ( double fovy, double n, double w, double h, int x, int y, double& X, double& Y, double& Z );


#endif // MATHSTUFF_H_INCLUDED