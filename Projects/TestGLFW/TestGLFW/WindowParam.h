#ifndef WINDOWPARAM_H_INCLUDED
#define WINDOWPARAM_H_INCLUDED


/////////////////////////////////////////////////
//
// Parameter structure
//
/////////////////////////////////////////////////
class CWinParam
{
    public:
//conf
        int             win_width;
        int             win_height;
        double          fovy;
        double          nearv;

//state
        int             width;
        int             height;

        int             mouse_x;
        int             mouse_y;
        int             mouse_x_previous;
        int             mouse_y_previous;

    CWinParam()
        :
        win_width(900),
        win_height(600),
        fovy(35.0),
        nearv(1.0)
    {

    }

	bool CWinParam::operator==(const CWinParam &other) const 
	{
		return 
			other.win_width == this->win_width
			&&
			other.win_height == this->win_height
			&&
			other.fovy == this->fovy
			&&
			other.nearv == this->nearv
			&&
			other.width == this->width
			&&
			other.height == this->height
			&&
			other.mouse_x == this->mouse_x
			&&
			other.mouse_y == this->mouse_y
			&&
			other.mouse_x_previous == this->mouse_x_previous
			&&
			other.mouse_y_previous == this->mouse_y_previous;
	}

	bool CWinParam::operator!=(const CWinParam &other) const 
	{
		return !(*this == other);
	}

};

#endif // WINDOWPARAM_H_INCLUDED
