# Microsoft Developer Studio Project File - Name="core" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** NICHT BEARBEITEN **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=core - Win32 Debug
!MESSAGE Dies ist kein g�ltiges Makefile. Zum Erstellen dieses Projekts mit NMAKE
!MESSAGE verwenden Sie den Befehl "Makefile exportieren" und f�hren Sie den Befehl
!MESSAGE 
!MESSAGE NMAKE /f "core.mak".
!MESSAGE 
!MESSAGE Sie k�nnen beim Ausf�hren von NMAKE eine Konfiguration angeben
!MESSAGE durch Definieren des Makros CFG in der Befehlszeile. Zum Beispiel:
!MESSAGE 
!MESSAGE NMAKE /f "core.mak" CFG="core - Win32 Debug"
!MESSAGE 
!MESSAGE F�r die Konfiguration stehen zur Auswahl:
!MESSAGE 
!MESSAGE "core - Win32 Release" (basierend auf  "Win32 (x86) Static Library")
!MESSAGE "core - Win32 Debug" (basierend auf  "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "core - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /W3 /GR /GX /O2 /I "../../../libs/mmgr" /I "../../../libs" /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "core - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GR /GX /ZI /Od /I "../../../libs" /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x407 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "core - Win32 Release"
# Name "core - Win32 Debug"
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\Alignment.h
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\Enumeration.h
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\OpenGL.h
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\Position.h
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\Position.inl
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\Rectangle.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\Rectangle.h
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\Serialization.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\Serialization.h
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\StandardException.h
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\tinystr.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\tinystr.h
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\tinyxml.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\tinyxml.h
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\tinyxmlerror.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\tinyxmlparser.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\Utils.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\Utils.h
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\Vector2.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\Vector2.h
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\Vector2.inl
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\XMLInputArchive.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\XMLInputArchive.h
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\XMLOutputArchive.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\libs\glgooey\core\XMLOutputArchive.h
# End Source File
# End Target
# End Project
