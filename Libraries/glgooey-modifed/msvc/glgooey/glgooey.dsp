# Microsoft Developer Studio Project File - Name="glgooey" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** NICHT BEARBEITEN **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=glgooey - Win32 Debug
!MESSAGE Dies ist kein g�ltiges Makefile. Zum Erstellen dieses Projekts mit NMAKE
!MESSAGE verwenden Sie den Befehl "Makefile exportieren" und f�hren Sie den Befehl
!MESSAGE 
!MESSAGE NMAKE /f "glgooey.mak".
!MESSAGE 
!MESSAGE Sie k�nnen beim Ausf�hren von NMAKE eine Konfiguration angeben
!MESSAGE durch Definieren des Makros CFG in der Befehlszeile. Zum Beispiel:
!MESSAGE 
!MESSAGE NMAKE /f "glgooey.mak" CFG="glgooey - Win32 Debug"
!MESSAGE 
!MESSAGE F�r die Konfiguration stehen zur Auswahl:
!MESSAGE 
!MESSAGE "glgooey - Win32 Release" (basierend auf  "Win32 (x86) Static Library")
!MESSAGE "glgooey - Win32 Debug" (basierend auf  "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "glgooey - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /W3 /GR /GX /O2 /I "../../../../libs/glgooey/services/interfaces" /I "../../../../libs/glgooey/core" /I "../../../../libs/mmgr" /I "../../libs" /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "glgooey - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GR /GX /ZI /Od /I "../../libs" /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x407 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "glgooey - Win32 Release"
# Name "glgooey - Win32 Debug"
# Begin Source File

SOURCE=..\..\libs\glgooey\BaseLayouter.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\BasicButton.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\BasicButton.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\BasicButtonGroup.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\BasicButtonGroup.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\BoxLayouter.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\BoxLayouter.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\Button.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\Button.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\CheckBox.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\CheckBox.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\CheckBoxGroup.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\CheckBoxGroup.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\Color.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\Color.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\ComboBox.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\ComboBox.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\ComplexGridLayouter.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\ComplexGridLayouter.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\Dock.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\Dock.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\docu.inl
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\EditField.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\EditField.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\EditMessage.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\FlowLayouter.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\FlowLayouter.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\FrameWindow.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\FrameWindow.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\GridLayouter.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\GridLayouter.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\KeyMessage.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\KeyMessage.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\ListBox.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\ListBox.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\ListControl.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\ListControl.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\ListWindow.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\ListWindow.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\Message.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\Message.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\MessageListener.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\MouseMessage.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\MouseMessage.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\MultiTextButton.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\MultiTextButton.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\Panel.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\Panel.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\ProgressBar.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\ProgressBar.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\PropertyScheme.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\PropertyScheme.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\Readers.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\RefCountedObject.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\RenderDesc.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\Renderer.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\Renderer.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\ScrollBar.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\ScrollBar.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\SelectionMessage.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\sigslot.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\Slideable.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\Slideable.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\SlideableMessage.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\Slider.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\Slider.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\SolidSurfaceDesc.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\SolidSurfaceDesc.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\SpinBox.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\SpinBox.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\StaticText.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\StaticText.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\TimeManager.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\TimeManager.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\Timer.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\Timer.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\Window.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\Window.h
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\WindowManager.cpp
# End Source File
# Begin Source File

SOURCE=..\..\libs\glgooey\WindowManager.h
# End Source File
# End Target
# End Project
