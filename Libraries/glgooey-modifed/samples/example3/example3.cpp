// __________________________________________________________________________________________________
//    GLGooey Graphical User Interface for OpenGL
//    Copyright (c) 2004 Niel Waldren
//
// This software is provided 'as-is', without any express or implied warranty. In no event will
// the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose, including commercial
// applications, and to alter it and redistribute it freely, subject to the following restrictions:
//
//     1. The origin of this software must not be misrepresented; you must not claim that you
//        wrote the original software. If you use this software in a product, an acknowledgment
//        in the product documentation would be appreciated but is not required.
//
//     2. Altered source versions must be plainly marked as such, and must not be misrepresented
//        as being the original software.
//
//     3. This notice may not be removed or altered from any source distribution.
//
// __________________________________________________________________________________________________




// *********************************************************************
//  a pretty crude app showing off different Gooey windows
// *********************************************************************

#include <cstdlib>
#include <GL/glut.h>

#include "glgooey/core/Rectangle.h"

#include "glgooey/WindowManager.h"
#include "glgooey/TimeManager.h"
#include "glgooey/SpinBox.h"
#include "glgooey/PropertyScheme.h"

#include "SettingsWindow.h"

using namespace Gooey;

int main_window;

#include <iostream>
#include <vector>
#include <fstream>





// A frame window with a list box inside it
class ListBoxWindow : public FrameWindow
{
public:
    ListBoxWindow() :
        FrameWindow(Core::Rectangle(430, 40, 700, 200), 0, "Console"),
        listBox_(0, Core::Rectangle(430, 40, 600, 400))
    {
        for(int j = 0; j < 10; j++)
        {
            listBox_.addString("Entry No. " + Gooey::Core::toString(j));
        }
        enableMovement();
        setClientWindow(&listBox_);
    }
private:
    ListBox listBox_;
};




// A frame window with a list control inside it
class ListControlWindow : public FrameWindow
{
public:
    ListControlWindow() :
        FrameWindow(Core::Rectangle(430, 220, 750, 400), 0, "List Control"),
        listControl_(0, Core::Rectangle(), 3)
    {
        for(int i = 0; i < 60; i++)
        {
            std::vector<std::string> strings;
            strings.push_back("first" + Gooey::Core::toString(i));
            strings.push_back("second" + Gooey::Core::toString(i));
            strings.push_back("third" + Gooey::Core::toString(i));
            listControl_.addRow(strings);
        }
        enableMovement();
        setClientWindow(&listControl_);
    }
private:
    ListControl listControl_;
};



// A frame window with some separately skinned buttons in it
class MainMenu : public FrameWindow
{
public:
    MainMenu() :
        FrameWindow(Core::Rectangle(710, 40, 900, 300), 0, "Main Menu"),
        panel_(Core::Rectangle(10, 10, 20, 20), 0),
        newButton_(0, ""),
        loadButton_(0, ""),
        saveButton_(0, ""),
        quitButton_(0, "")
    {
        newButton_.loadAppearance("../../samples/data/example3.xml", "newButton");
        loadButton_.loadAppearance("../../samples/data/example3.xml", "loadButton");
        saveButton_.loadAppearance("../../samples/data/example3.xml", "saveButton");
        quitButton_.loadAppearance("../../samples/data/example3.xml", "quitButton");

        panel_.loadAppearance("../../samples/data/example3.xml", "mainMenuPanel");

        ComplexGridLayouter* layouter = new ComplexGridLayouter;
        panel_.setLayouter(layouter);
        std::vector<float> widths;
        widths.push_back(0.0f);    // This means use whatever width is available
        layouter->setColumnWidths(widths);

        std::vector<float> heights;
        for(int k = 0; k < 4; ++k) heights.push_back(48.0f);
        heights.push_back(0.0f);
        layouter->setRowHeights(heights);

        ComplexGridCellInfo info;

        info.columnIndex = 0; info.rowIndex = 0; panel_.addChildWindow(&newButton_, info);
        info.columnIndex = 0; info.rowIndex = 1; panel_.addChildWindow(&loadButton_, info);
        info.columnIndex = 0; info.rowIndex = 2; panel_.addChildWindow(&saveButton_, info);
        info.columnIndex = 0; info.rowIndex = 3; panel_.addChildWindow(&quitButton_, info);

        enableMovement();
        setClientWindow(&panel_);
    }


private:
    Panel panel_;
    Button newButton_;
    Button loadButton_;
    Button saveButton_;
    Button quitButton_;
};



SettingsWindow*    settingsWindow = 0;
ListBoxWindow*     listBoxWindow = 0;
ListControlWindow* listControlWindow = 0;
MainMenu*          mainMenu = 0;



// *************************************************************************
//  redraw in idle time
// *************************************************************************

void myGlutIdle( void )
{
    if ( glutGetWindow() != main_window )
        glutSetWindow(main_window);

    glutPostRedisplay();
}



// *************************************************************************
//   this is where most of the integration work is done
//  myGlutMouse, myGlutMotion, myGlutKey, myGlutSpecial all send
//  their respective messages on to the window manager
// *************************************************************************
void myGlutMouse(int button, int state, int x, int y)
{
    switch(button)
    {
    case GLUT_LEFT_BUTTON:
        if(state == GLUT_UP)
            WindowManager::instance().onLeftButtonUp(x, y);
        else
            WindowManager::instance().onLeftButtonDown(x, y);
        break;
    case GLUT_MIDDLE_BUTTON:
        if(state == GLUT_UP)
            WindowManager::instance().onMiddleButtonUp(x, y);
        else
            WindowManager::instance().onMiddleButtonDown(x, y);
        break;
    case GLUT_RIGHT_BUTTON:
        if(state == GLUT_UP)
            WindowManager::instance().onRightButtonUp(x, y);
        else
            WindowManager::instance().onRightButtonDown(x, y);
        break;
    }
}



void myGlutMotion(int x, int y)
{
    WindowManager::instance().onMouseMove(x, y);
}


void myGlutKey(unsigned char key, int /*x*/, int /*y*/)
{
    WindowManager::instance().onChar(key);
}

void myGlutSpecial(int key, int /*x*/, int /*y*/)
{
    WindowManager::instance().onKeyDown(key);
}

bool isShiftPressed()
{
    return (glutGetModifiers() & GLUT_ACTIVE_SHIFT) != 0;
}

bool isAltPressed()
{
    return (glutGetModifiers() & GLUT_ACTIVE_ALT) != 0;
}

bool isCtrlPressed()
{
    return (glutGetModifiers() & GLUT_ACTIVE_CTRL) != 0;
}




// *************************************************************************
//   We're just going to draw a wireframe sphere in the center of the screen
//  so when a resize happens we reset the projection - not forgetting to
//  let the window manager know that the window it's running in has been

//  resized
// *************************************************************************
void myGlutReshape( int x, int y )
{
    WindowManager::instance().applicationResized(x, y);

    glViewport( 0, 0, x, y );

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(90, 2, 1, 100);
    gluLookAt(2, 2, 2, 0, 0, 0, 0, 1, 0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();


    glutPostRedisplay();
}




// *************************************************************************
//  the once per frame update
// *************************************************************************
void myGlutDisplay( void )
{
    // ---------------------- All this stuff is initialization --------------------

    // need this for 3d stuff in the background
    static GLUquadricObj* quadObj = 0;

    if(quadObj == 0)
    {
        quadObj = gluNewQuadric ();
        gluQuadricDrawStyle(quadObj, GLU_LINE);
    }


    // --------------- This is where the actual frame starts -----------------

    // clear the screen
    glClearColor( 0.1, 0.1, 0.1, 1.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    // draw a wireframe sphere
    glLoadIdentity();
    glRotatef(TimeManager::systemTime()*10.0f, 0, 1, 0);
    glColor4f( 0.4, 0.6, 0.6, 1.0f );
    gluSphere (quadObj, 2, 16, 16);

    // this must be called once per frame
    WindowManager::instance().update();

    // do some user defined stuff in the settings window
    settingsWindow->update();

    // swap the buffers
    glutSwapBuffers();

}



void shutDown()
{
    WindowManager::instance().removeAllWindows();

    delete settingsWindow;
    delete listBoxWindow;
    delete listControlWindow;
    delete mainMenu;

    WindowManager::instance().terminate();
}


// *************************************************************************
//  main
// *************************************************************************
int main(int argc, char* argv[])
{
    // ShutDown should be called when the program exits
    atexit(shutDown);

    // *********************************************************************
    //   Initialize GLUT and create window
    // *********************************************************************

    glutInit(&argc, argv);
    glutInitDisplayMode( GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH );
    glutInitWindowPosition( 50, 50 );
    glutInitWindowSize( 900, 500 );

    main_window = glutCreateWindow( "Gooey Example3" );

    glutDisplayFunc ( myGlutDisplay );
    glutReshapeFunc ( myGlutReshape );
    glutIdleFunc    ( myGlutDisplay );
    glutMouseFunc   ( myGlutMouse   );
    glutKeyboardFunc( myGlutKey     );
    glutSpecialFunc ( myGlutSpecial );
    glutPassiveMotionFunc ( myGlutMotion  );
    glutMotionFunc  ( myGlutMotion  );

    WindowManager::instance().initialize("../../samples/data/accid___.ttf", isShiftPressed, isAltPressed, isCtrlPressed);
    //WindowManager::instance().propertyScheme().load("../../samples/data/HighContrast.xml");

    // The only reason these are created on the heap is because GLUT just exits the program when
    // the window system shuts the application down. So any objects created on main's stack are
    // not cleaned up. By creating the objects on the heap we can use "atexit" to register a
    // callback which deletes these objects before the program terminates.
    settingsWindow = new SettingsWindow;
    listBoxWindow = new ListBoxWindow;
    listControlWindow = new ListControlWindow;
    mainMenu = new MainMenu;

    WindowManager::instance()
        .addWindow(settingsWindow)
        .addWindow(listBoxWindow)
        .addWindow(listControlWindow)
        .addWindow(mainMenu)
        ;

    try
    {
        glutMainLoop();
    }
    catch(std::exception& e)
    {
        std::cout << "Exception: " << e.what() << std::endl;
        std::cin.get();
    }

    return 0;
}
