// __________________________________________________________________________________________________
//    GLGooey Graphical User Interface for OpenGL
//    Copyright (c) 2004 Niel Waldren
//
// This software is provided 'as-is', without any express or implied warranty. In no event will
// the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose, including commercial
// applications, and to alter it and redistribute it freely, subject to the following restrictions:
//
//     1. The origin of this software must not be misrepresented; you must not claim that you
//        wrote the original software. If you use this software in a product, an acknowledgment
//        in the product documentation would be appreciated but is not required.
//
//     2. Altered source versions must be plainly marked as such, and must not be misrepresented
//        as being the original software.
//
//     3. This notice may not be removed or altered from any source distribution.
//
// __________________________________________________________________________________________________

#ifndef __SETTINGS_WINDOW__H__
#define __SETTINGS_WINDOW__H__

#include "glgooey/FrameWindow.h"
#include "glgooey/CheckBox.h"
#include "glgooey/CheckBoxGroup.h"
#include "glgooey/Button.h"
#include "glgooey/Panel.h"
#include "glgooey/EditField.h"
#include "glgooey/MultiTextButton.h"
#include "glgooey/ScrollBar.h"
#include "glgooey/ListBox.h"
#include "glgooey/ListControl.h"
#include "glgooey/StaticText.h"
#include "glgooey/ProgressBar.h"
#include "glgooey/Slider.h"
#include "glgooey/ComplexGridLayouter.h"

namespace Gooey
{

//  This is the main settings window - It is an example of how a window with a complex layout
// and several different control types can be created.
class SettingsWindow : public FrameWindow
{
public:
    SettingsWindow() :
        FrameWindow(Core::Rectangle(0, 0, 400, 420), 0, "Settings"),
        panel_(Core::Rectangle(10, 10, 20, 20), 0),
        editField_(0, "Edit Field:"),
        spinBox_(0, 23),
        standardButton_(0, "Push Button"),
        multiTextButton_(0, "Multi-Text Button"),
        coloredButton_(0, "A Colored Button"),
        staticText_(0, "Number of Frames per Second:"),
        slider_(0, Slider::horizontal, Core::Rectangle(10, 10, 20, 20)),
        progressBar_(0, Core::Rectangle()),
        progressBar1_(0, Core::Rectangle(), ProgressBar::vertical),
        radioButton1_(0, "Radio Button 1", &radioButtons_),
        radioButton2_(0, "Radio Button 2", &radioButtons_),
        radioButton3_(0, "Radio Button 3", &radioButtons_),
        radioButton4_(0, "Radio Button 4", &radioButtons_)
    {
        // load the colored button's appearance
        coloredButton_.loadAppearance("../../samples/data/example3.xml", "coloredButton");

        // add some strings to the multitext button
        multiTextButton_
            .addString("An Option")
            .addString("Another Option")
            .addString("And Another Option")
            .addString("Another Option 1")
            .addString("Another Option 2")
            .addString("Another Option 3")
            ;

        // Initialize the slider
        slider_
            .setTickFrequency(2)
            .setTickPlacement(Slider::both)
            .setMaximum(20)
            .setBlockIncrement(4)
            ;

        // Select one of the radio buttons
        radioButtons_.select(&radioButton1_);

        // Here's how to use a complex grid layout...

        // create the layout object and tell the panel to use it
        ComplexGridLayouter* layouter = new ComplexGridLayouter;
        panel_.setLayouter(layouter);

        //  choose the widths of the columns - Here two columns are used. The
        // first one is 150 pixels wide, the third one is 40 pixels wide. The
        // second column is set to 0 and thus fills whatever space is left.
        std::vector<float> widths;
        widths.push_back(150.0f);
        widths.push_back(0.0f);
        widths.push_back(40.0f);
        layouter->setColumnWidths(widths);

        // set the row heights - This is basically the same procedure as above:
        // 8 rows with heights: 42, 42, 42, 64, 0, 42, 42, 42
        // whereby 0 means use as much space as is available. The progress bar
        // is therefore expanded such that the whole panel is filled vertically.
        std::vector<float> heights;
        for(int k1 = 0; k1 < 3; ++k1) heights.push_back(42.0f);
        heights.push_back(64.0f);
        heights.push_back(0.0f);
        heights.push_back(42.0f);
        for(int k2 = 0; k2 < 2; ++k2) heights.push_back(36.0f);
        layouter->setRowHeights(heights);

        // Now we create a ComplexGridCellInfo instance...
        ComplexGridCellInfo info;

        // ...and before each child is added to the panel_, the info is
        // updated to put the child in the right place
        info.columnIndex = 0; info.rowIndex = 0;
        panel_.addChildWindow(&editField_, info);

        info.columnIndex = 1; info.rowIndex = 0;
        panel_.addChildWindow(&spinBox_, info);

        info.columnIndex = 2; info.rowIndex = 0; info.verticalSpan = int(heights.size());
        panel_.addChildWindow(&progressBar1_, info);

        info.columnIndex = 0; info.rowIndex = 1; info.verticalSpan = 1;
        panel_.addChildWindow(&standardButton_, info);

        info.columnIndex = 1; info.rowIndex = 1;
        panel_.addChildWindow(&multiTextButton_, info);

        info.columnIndex = 0; info.rowIndex = 2; info.horizontalSpan = 2;
        panel_.addChildWindow(&coloredButton_, info);

        info.columnIndex = 0; info.rowIndex = 3; info.horizontalSpan = 1;
        panel_.addChildWindow(&staticText_, info);

        info.columnIndex = 1; info.rowIndex = 3;
        info.horizontalSpacing = 20.0f; info.verticalSpacing = 2.0f;
        info.columnIndex = 0; info.rowIndex = 4;
        info.horizontalSpan = 2; info.verticalSpan = 1;
        info.horizontalSpacing = 58.0f; info.verticalSpacing = 8.0f;
        panel_.addChildWindow(&progressBar_, info);

        info.columnIndex = 0; info.rowIndex = 5;
        info.horizontalSpacing = 8.0f;
        panel_.addChildWindow(&slider_, info);

        info.columnIndex = 0; info.rowIndex = 6; info.horizontalSpan = 1;
        info.horizontalSpan = 1; info.verticalSpan = 1;
        info.horizontalSpacing = 6.0f; info.verticalSpacing = 6.0f;
        panel_.addChildWindow(&radioButton1_, info);

        info.columnIndex = 1; info.rowIndex = 6;
        panel_.addChildWindow(&radioButton2_, info);

        info.columnIndex = 0; info.rowIndex = 7;
        panel_.addChildWindow(&radioButton3_, info);

        info.columnIndex = 1; info.rowIndex = 7;
        panel_.addChildWindow(&radioButton4_, info);

        // Now enable movement and set the panel as the client window of the settings window
        enableMovement();
        setClientWindow(&panel_);
    }

    virtual ~SettingsWindow() {}


public:
    void update()
    {
        // ---------------------- Frame rate calculation stuff ----------------------
        static int frameCount = 0;
        static int lastUpdateTime = int(TimeManager::systemTime() * 1000.0);

        int now = int(TimeManager::systemTime() * 1000.0);
        if(now - lastUpdateTime > 1000)
        {
            const float fps = float(frameCount) / (float(now - lastUpdateTime) / 1000.0);
            // put the text in the static text field
            staticText_.setText("Number of frames per second: " + Core::toString(int(fps)));
            lastUpdateTime = now;
            frameCount = 0;
        }

        frameCount++;

        // ------------------ Play around with the progress bars -------------------
        static bool increasing = true;
        if(rand() % 100 == 0) increasing = !increasing;
        const int val = increasing ? 1 : -1;
        progressBar_.setPercentage(progressBar_.percentage() + val);
        progressBar1_.setPercentage(float(spinBox_.value()) / 2.55f);
    }

private:
    Panel           panel_;
    EditField       editField_;
    SpinBox         spinBox_;
    Button          standardButton_;
    MultiTextButton multiTextButton_;
    Button          coloredButton_;
    StaticText      staticText_;
    Slider          slider_;
    ProgressBar     progressBar_;
    ProgressBar     progressBar1_;
    CheckBoxGroup   radioButtons_;
    CheckBox        radioButton1_;
    CheckBox        radioButton2_;
    CheckBox        radioButton3_;
    CheckBox        radioButton4_;
};

}


#endif
