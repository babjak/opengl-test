// __________________________________________________________________________________________________
//    GLGooey Graphical User Interface for OpenGL
//    Copyright (c) 2004 Niel Waldren
//
// This software is provided 'as-is', without any express or implied warranty. In no event will
// the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose, including commercial
// applications, and to alter it and redistribute it freely, subject to the following restrictions:
//
//     1. The origin of this software must not be misrepresented; you must not claim that you
//        wrote the original software. If you use this software in a product, an acknowledgment
//        in the product documentation would be appreciated but is not required.
//
//     2. Altered source versions must be plainly marked as such, and must not be misrepresented
//        as being the original software.
//
//     3. This notice may not be removed or altered from any source distribution.
//
// __________________________________________________________________________________________________




// *********************************************************************
//  a small app showing how to respond to button pressing
//  The idea is a small console with an input edit field. When the user
//  presses a button, the contents of the edit field are written
//  into a list box.
// *********************************************************************

#include "mmgr/nommgr.h"

#include <cstdlib>
#include <iostream>
#include <GL/glut.h>

#include "mmgr/mmgr.h"

#include "glgooey/core/StandardException.h"
#include "glgooey/core/Utils.h"
#include "glgooey/core/Rectangle.h"

#include "glgooey/WindowManager.h"
#include "glgooey/FrameWindow.h"
#include "glgooey/Button.h"
#include "glgooey/Panel.h"
#include "glgooey/EditField.h"
#include "glgooey/ListBox.h"
#include "glgooey/TimeManager.h"
#include "glgooey/PropertyScheme.h"



using namespace Gooey;

int main_window;

// **************************************************************************************************
//  The main GLGooey window is a moveable frame window with an edit field, a list box and a
// "done" button in it.
//  Notice how the child windows can be created in the constructor's initialization list, and the
// "Parent" parameter is just set to 0 (as in editField_(0, "EditField") ). We can do this because
// the child<->parent relationships are set up later on when the panel is set as the frame window's
// client window and the other windows are added to the panel. When the panel is set as the frame
// window's client window, this also resizes the panel to fit just inside the frame window, which
// explains why the window rectangle passed to the panel's constructor needn't make sense.
// **************************************************************************************************
class Console : public FrameWindow
{
public:
    Console() :
        FrameWindow(Core::Rectangle(50, 50, 400, 400), 0, "Console"),
        panel_(Core::Rectangle(10, 10, 20, 20), 0),
        editField_(0, "Edit Field:"),
        listBox_(0, Core::Rectangle(0, 0, 600, 240)),
        doneButton_(0, "Done")
    {
        // Leave this out and the user will not be able to drag the window around by its frame
        enableMovement();

        // set the edit field's size
        editField_.setSize(Core::Vector2(220, 30));

        // add some dummy strings to the list box
        for(int i = 0; i < 20; ++i)
        {
            listBox_.addString("testing" + Core::toString(i));
        }

        // set the "done" buttton's size
        doneButton_.setSize(Core::Vector2(55, 30));

        // wire up the signal/slot connections - pressing Return in the edit field should have
        // the same effect as pressing the done button
        doneButton_.pressed.connect(this, &Console::doneButtonPressed);
        editField_.returnPressed.connect(this, &Console::doneButtonPressed);

        // put the controls in the panel
        panel_
            .addChildWindow(&listBox_)
            .addChildWindow(&editField_)
            .addChildWindow(&doneButton_)
            ;

        // put the panel in the frame window
        setClientWindow(&panel_);

        // arrange the panel's children according to the default flow layouter
        panel_.arrangeChildren();
    }


public:
    // this is the slot that responds to the button being pressed
    void doneButtonPressed()
    {
        // get the edit field's text, add it to the list box, then clear the edit field
        std::string str = editField_.text();
        if(str != "") listBox_.addString(str);
        editField_.setText("");
    }

private:
    // the controls
    Panel       panel_;
    EditField   editField_;
    ListBox     listBox_;
    Button      doneButton_;
};

// This has been made global for simplicity!
Console* console = 0;


// **************************************************************************************************
//  redraw in idle time - there's nothing GLGooey related here
// **************************************************************************************************
void myGlutIdle( void )
{
    if ( glutGetWindow() != main_window )
        glutSetWindow(main_window);

    glutPostRedisplay();
}


// **************************************************************************************************
//   this is where most of the integration work is done
//  myGlutMouse, myGlutMotion, myGlutKey, myGlutSpecial all send
//  their respective messages on to the window manager
// **************************************************************************************************
void myGlutMouse(int button, int state, int x, int y)
{
    switch(button)
    {
    case GLUT_LEFT_BUTTON:
        if(state == GLUT_UP)
            WindowManager::instance().onLeftButtonUp(x, y);
        else
            WindowManager::instance().onLeftButtonDown(x, y);
        break;
    case GLUT_MIDDLE_BUTTON:
        if(state == GLUT_UP)
            WindowManager::instance().onMiddleButtonUp(x, y);
        else
            WindowManager::instance().onMiddleButtonDown(x, y);
        break;
    case GLUT_RIGHT_BUTTON:
        if(state == GLUT_UP)
            WindowManager::instance().onRightButtonUp(x, y);
        else
            WindowManager::instance().onRightButtonDown(x, y);
        break;
    }
}


void myGlutMotion(int x, int y)
{
    WindowManager::instance().onMouseMove(x, y);
}


void myGlutKey(unsigned char key, int , int )
{
    WindowManager::instance().onChar(key);
}

void myGlutSpecial(int key, int , int )
{
    WindowManager::instance().onKeyDown(key);
}




// **************************************************************************************************
//  In order to let GLGooey know whether one of the modifier keys have been pressed, we must define
//  three functions and pass them to the window manager on initialization. These are the three
//  functions - they simply return true iff the corresponding key is pressed
// **************************************************************************************************
bool isShiftPressed()
{
    return (glutGetModifiers() & GLUT_ACTIVE_SHIFT) != 0;
}

bool isAltPressed()
{
    return (glutGetModifiers() & GLUT_ACTIVE_ALT) != 0;
}

bool isCtrlPressed()
{
    return (glutGetModifiers() & GLUT_ACTIVE_CTRL) != 0;
}








// **************************************************************************************************
//   We're just going to draw a wireframe sphere in the center of the screen
//  so when a resize happens we reset the projection - not forgetting to
//  let the window manager know that the window it's running in has been
//  resized.
//   NOTE: One of the most common errors for people starting out with GLGooey is to
// forget to inform the window manager when the application window is resized!
// **************************************************************************************************
void myGlutReshape( int x, int y )
{
    WindowManager::instance().applicationResized(x, y);

    glViewport( 0, 0, x, y );

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(90, 1, 1, 100);
    gluLookAt(2, 2, 2, 0, 0, 0, 0, 1, 0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glutPostRedisplay();
}




// **************************************************************************************************
//  the once per frame update
// **************************************************************************************************
void myGlutDisplay( void )
{
    // ------------------------- This stuff is initialization --------------------

    // need this for 3d stuff in the background
    static GLUquadricObj* quadObj = 0;
    if(quadObj == 0)
    {
        quadObj = gluNewQuadric();
        gluQuadricDrawStyle(quadObj, GLU_LINE);
    }


    // ---------------------- This is the background and the 3D scene --------------------

    // clear the screen
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    // draw a wireframe cone
    glLoadIdentity();
    glRotatef(TimeManager::systemTime()*10.0f, 1, 0, 0);
    glColor4f( 1, 1, 1, 1 );

    gluCylinder (quadObj, 1.6, 0.01, 2.5, 16, 16);


    // ---------------------- This is all that's needed to keep GLGooey running --------------------

    // this must be called once per frame
    WindowManager::instance().update();


    // ---------------------- Finally the buffers are swapped --------------------

    glutSwapBuffers();
}



// **************************************************************************************************
//   Because GLUT does not continue the execution of main() when the application window is
//  closed we have to use the atexit callback and do our cleaning up in the corresponding function.
// **************************************************************************************************
void shutDown()
{
    WindowManager::instance().removeWindow(console);

    delete console;

    WindowManager::instance().terminate();
}



// **************************************************************************************************
//  main
// **************************************************************************************************
int main(int argc, char* argv[])
{
    atexit(shutDown);

    // **********************************************************************************************
    //   Initialize GLUT and create window
    // **********************************************************************************************
    glutInit(&argc, argv);
    glutInitDisplayMode( GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH );
    glutInitWindowPosition( 50, 50 );
    glutInitWindowSize( 450, 450 );

    main_window = glutCreateWindow( "Gooey Example1" );

    glutDisplayFunc ( myGlutDisplay );
    glutReshapeFunc ( myGlutReshape );
    glutIdleFunc    ( myGlutDisplay );
    glutMouseFunc   ( myGlutMouse   );
    glutKeyboardFunc( myGlutKey     );
    glutSpecialFunc ( myGlutSpecial );
    glutPassiveMotionFunc ( myGlutMotion  );
    glutMotionFunc  ( myGlutMotion  );

    glClearColor( 0, 0.2, 0.2, 1.0f );


    // **********************************************************************************************
    //  Initialize GLGooey - The parameters are:
    //   - The default font to use
    //   - The three modifier key functions defined above
    // **********************************************************************************************
    WindowManager::instance().initialize("../../samples/data/accid___.ttf", isShiftPressed, isAltPressed, isCtrlPressed);

    // **********************************************************************************************
    //   Create an instance of the GLGooey window defined in the Console class and add it to the
    //  window manager. Once it has been added it will be drawn every time
    //  WindowManager::instance().update() is called
    // **********************************************************************************************
    console = new Console;
    WindowManager::instance().addWindow(console);

    // **********************************************************************************************
    //  Start the main loop
    // **********************************************************************************************
    try
    {
        glutMainLoop();
    }
    catch(std::exception& e)
    {
        std::cout << "Exception: " << e.what() << std::endl;
        std::cin.get();
    }

    return 0;
}
