// __________________________________________________________________________________________________
//    GLGooey Graphical User Interface for OpenGL
//    Copyright (c) 2004 Niel Waldren
//
// This software is provided 'as-is', without any express or implied warranty. In no event will
// the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose, including commercial
// applications, and to alter it and redistribute it freely, subject to the following restrictions:
//
//     1. The origin of this software must not be misrepresented; you must not claim that you
//        wrote the original software. If you use this software in a product, an acknowledgment
//        in the product documentation would be appreciated but is not required.
//
//     2. Altered source versions must be plainly marked as such, and must not be misrepresented
//        as being the original software.
//
//     3. This notice may not be removed or altered from any source distribution.
//
// __________________________________________________________________________________________________


#include "mmgr/nommgr.h"

#include <algorithm>

#include <sys/types.h>
#include <sys/stat.h>

#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/convenience.hpp>

#include "mmgr/mmgr.h"

#include "FileDialog.h"

#include "glgooey/core/Rectangle.h"
#include "glgooey/Panel.h"
#include "glgooey/StaticText.h"
#include "glgooey/Button.h"
#include "glgooey/ComboBox.h"
#include "glgooey/ListBox.h"
#include "glgooey/ComplexGridLayouter.h"


using namespace Gooey;

namespace fs = boost::filesystem;

// *****************************************************************************
//  used to sort file names in the list box
// *****************************************************************************
static bool compareFileNames(const FileDialog::Entry& lhs, const FileDialog::Entry& rhs)
{
    bool ret = false;
    if(lhs.isDirectory && !rhs.isDirectory)
        ret = true;
    else if(!lhs.isDirectory && rhs.isDirectory)
        ret = false;
    else
    {
        ret = lhs.name < rhs.name;
    }
    return ret;
}


// *****************************************************************************
//  Constructor
//  \param title the title of the frame window which is the dialog
//  \param directory the directory to start searching from
//  \param filter an array of filters which may be used to filter the files
//            shown in the list (an empty list indicates that all files
//            should be shown).
//  \param type select either file or directory selection using this value
// *****************************************************************************
FileDialog::FileDialog(const std::string& title, const std::string& directory,
    Type type, const Strings& aFilter) :
        FrameWindow(Gooey::Core::Rectangle(20, 50, 640, 400), 0, title),
        type_(type)
{
    // *************************************************************************
    //  Create the windows
    // *************************************************************************

    // create the panel
    panel_ = new Panel(Gooey::Core::Rectangle(10, 10, 20, 20), this);

    // create the list box
    listBox_ = new ListBox(panel_, Gooey::Core::Rectangle(10, 10, 20, 20));

    // create the layout object and tell the panel to use it
    ComplexGridLayouter* layouter = new ComplexGridLayouter;
    panel_->setLayouter(layouter);

    // create the labels
    directoryLabel_ = new StaticText(panel_, "Directory");
    fileNameLabel_  = new StaticText(panel_, "File Name");
    filterLabel_    = new StaticText(panel_, "Filter");

    // create the combo boxes
    directoryCombo_ = new ComboBox(panel_, Gooey::Core::Rectangle(10, 10, 20, 20));
    filterCombo_    = new ComboBox(panel_, Gooey::Core::Rectangle(10, 10, 20, 20));
    fileNameCombo_  = new ComboBox(panel_, Gooey::Core::Rectangle(10, 10, 20, 20));

    // create the buttons
    goButton_       = new Button(panel_, "Go");
    upButton_       = new Button(panel_, "Up");
    nextButton_     = new Button(panel_, "Next");
    previousButton_ = new Button(panel_, "Previous");
    okButton_       = new Button(panel_, "OK");
    cancelButton_   = new Button(panel_, "Cancel");


    // *************************************************************************
    //  Arrange the layout
    // *************************************************************************

    //  choose the widths of the columns
    std::vector<float> widths;
    widths.push_back(80.0f);
    widths.push_back(0.0f);
    widths.push_back(80.0f);
    layouter->setColumnWidths(widths);

    // set the row heights
    std::vector<float> heights;
    for(int i = 0; i < 4; ++i) heights.push_back(30.0f);
    heights.push_back(0.0f);
    heights.push_back(30.0f);
    heights.push_back(30.0f);
    layouter->setRowHeights(heights);

    // create a ComplexGridCellInfo instance
    ComplexGridCellInfo info;

    // the top row
    info.columnIndex = 0; info.rowIndex = 0;
    panel_->addChildWindow(directoryLabel_, info);
    info.columnIndex = 1; info.rowIndex = 0;
    panel_->addChildWindow(directoryCombo_, info);
    info.columnIndex = 2; info.rowIndex = 0;
    panel_->addChildWindow(goButton_, info);

    // list box and buttons in the middle
    info.columnIndex = 0; info.rowIndex = 1; info.horizontalSpan = 2; info.verticalSpan = 4;
    panel_->addChildWindow(listBox_, info);
    info.columnIndex = 2; info.rowIndex = 1; info.horizontalSpan = 1; info.verticalSpan = 1;
    panel_->addChildWindow(upButton_, info);
    info.columnIndex = 2; info.rowIndex = 2;
    panel_->addChildWindow(nextButton_, info);
    info.columnIndex = 2; info.rowIndex = 3;
    panel_->addChildWindow(previousButton_, info);

    // the filename row
    info.columnIndex = 0; info.rowIndex = 5;
    panel_->addChildWindow(fileNameLabel_, info);
    info.columnIndex = 1; info.rowIndex = 5;
    panel_->addChildWindow(fileNameCombo_, info);
    info.columnIndex = 2; info.rowIndex = 5;
    panel_->addChildWindow(okButton_, info);

    // the filter row
    info.columnIndex = 0; info.rowIndex = 6;
    panel_->addChildWindow(filterLabel_, info);
    info.columnIndex = 1; info.rowIndex = 6;
    panel_->addChildWindow(filterCombo_, info);
    info.columnIndex = 2; info.rowIndex = 6;
    panel_->addChildWindow(cancelButton_, info);

    setClientWindow(panel_);

    panel_->arrangeChildren();


    // *************************************************************************
    //  Wire up signals and slots
    // *************************************************************************

    directoryCombo_->selectionChanged.connect(this, &FileDialog::onDirectoryChanged);
    filterCombo_->selectionChanged.connect(this, &FileDialog::onFilterChanged);
    listBox_->selectionChanged.connect(this, &FileDialog::onSelectionChanged);
    upButton_->clicked.connect(this, &FileDialog::onUpPressed);
    nextButton_->clicked.connect(this, &FileDialog::onNextPressed);
    previousButton_->clicked.connect(this, &FileDialog::onPreviousPressed);
    okButton_->clicked.connect(this, &FileDialog::onOKPressed);
    cancelButton_->clicked.connect(this, &FileDialog::onCancelPressed);


    // *************************************************************************
    //  Initialize Data
    // *************************************************************************

    for(Strings::const_iterator it = aFilter.begin(); it != aFilter.end(); ++it)
    {
        filterCombo_->addString(*it);
    }
    filterCombo_->addString("All Files, *.*");
    filterCombo_->selectStringAt(0);

    fs::path startPath = directory != "" ? fs::path(directory, fs::native) : fs::current_path();
    std::string dir = startPath.native_directory_string();

    directoryCombo_->selectString(dir);

    if(type == selectDirectories)
    {
        fileNameLabel_->hide();
        fileNameCombo_->hide();
        filterLabel_->hide();
        filterCombo_->hide();
    }
}






// *****************************************************************************
//  Destructor
// *****************************************************************************
FileDialog::~FileDialog()
{
    setClientWindow(0);

    delete panel_;
    delete listBox_;
    delete directoryLabel_;
    delete fileNameLabel_;
    delete filterLabel_;
    delete directoryCombo_;
    delete filterCombo_;
    delete fileNameCombo_;
    delete goButton_;
    delete upButton_;
    delete nextButton_;
    delete previousButton_;
    delete okButton_;
    delete cancelButton_;
}





// *****************************************************************************
//  returns the full path to the selected file
// *****************************************************************************
std::string FileDialog::fullPath() const
{
    fs::path stem = fs::path(directoryCombo_->selectedString(), fs::native);
    fs::path file = fs::path(fileNameCombo_->selectedString(), fs::native);
    return (stem / file).native_file_string();
}





// *****************************************************************************
//  returns the name of the selected directory
// *****************************************************************************
std::string FileDialog::directoryName() const
{
    return directoryCombo_->selectedString();
}





// *****************************************************************************
//  returns the name of the selected file
// *****************************************************************************
std::string FileDialog::fileName() const
{
    return fileNameCombo_->selectedString();
}






// *****************************************************************************
//  returns the name of the selected filter
// *****************************************************************************
std::string FileDialog::filter() const
{
    return filterCombo_->selectedString();
}





// *****************************************************************************
//  react to the directory changing in the directory combo box
// *****************************************************************************
FileDialog& FileDialog::onDirectoryChanged(const std::string& newDir)
{
    Entries entries;
    getDirectoryContents(newDir, entries);
    std::sort(entries.begin(), entries.end(), compareFileNames);
    listBox_->removeAllStrings();
    for(Entries::iterator it = entries.begin(); it != entries.end(); ++it)
    {
        listBox_->addString((*it).name);
    }
    fileNameCombo_->setText("");
    directoryChanged(directoryName());
    return *this;
}





// *****************************************************************************
//  react to the filter changing in the filter combo box
// *****************************************************************************
FileDialog& FileDialog::onFilterChanged(const std::string& )
{
    onDirectoryChanged(directoryCombo_->selectedString());
    return *this;
}





// *****************************************************************************
//  react to the selection changing in the list box
// *****************************************************************************
FileDialog& FileDialog::onSelectionChanged(int index)
{
    std::string name = listBox_->stringAt(index);
    if(*name.begin() == '[')
    {
        name.erase(name.begin());
        name.erase(name.end()-1);
        fs::path newPath = fs::path(directoryCombo_->selectedString(), fs::native) / fs::path(name, fs::native);
        directoryCombo_->selectString(newPath.native_directory_string());
    }
    else
    {
        fileNameCombo_->selectString(name);
        fileSelected(name);
    }

    return *this;
}





// *****************************************************************************
//  react to the "up" button being pressed
// *****************************************************************************
FileDialog& FileDialog::onUpPressed()
{
    std::string name = directoryCombo_->selectedString();

    fs::path selectedPath = fs::path(name, fs::native).branch_path();

    directoryCombo_->selectString(selectedPath.native_directory_string());

    return *this;
}






// *****************************************************************************
//  react to the "next" button being pressed
// *****************************************************************************
FileDialog& FileDialog::onNextPressed()
{
    int curIndex = directoryCombo_->selectedIndex();
    if((curIndex >= 0) && (curIndex < int(directoryCombo_->numberOfStrings())-1))
    {
        directoryCombo_->selectStringAt(curIndex+1);
    }
    return *this;
}




// *****************************************************************************
//  react to the "previous" button being pressed
// *****************************************************************************
FileDialog& FileDialog::onPreviousPressed()
{
    int curIndex = directoryCombo_->selectedIndex();
    if((curIndex > 0) && (curIndex < int(directoryCombo_->numberOfStrings())))
    {
        directoryCombo_->selectStringAt(curIndex-1);
    }
    return *this;
}





// *****************************************************************************
//  react to the "OK" button being pressed
// *****************************************************************************
FileDialog& FileDialog::onOKPressed()
{
    hide();
    okPressed();
    return *this;
}




// *****************************************************************************
//  react to the "Cancel" button being pressed
// *****************************************************************************
FileDialog& FileDialog::onCancelPressed()
{
    hide();
    cancelPressed();
    return *this;
}




// *****************************************************************************
//   Retrieves the contents of the passed in directory as an array of Entry
//  objects. Lack of posix support calls for system dependent code for
//  Windows.
// *****************************************************************************
void FileDialog::getDirectoryContents(const std::string& directory, Entries& entries)
{
    fs::path directoryPath = directory != "" ? fs::path(directory, fs::native) : fs::current_path().root_path();
    ASSERT(fs::exists(directoryPath));
    fs::directory_iterator endItr;
    for(fs::directory_iterator itr(directoryPath); itr != endItr; ++itr)
    {
        try
        {
            if(fs::is_directory(*itr))
            {
                entries.push_back(Entry(std::string("[") + (*itr).leaf() + "]", true));
            }
            else
            {
                if(isAccepted(*itr)) entries.push_back(Entry((*itr).leaf(), false));
            }
        }
        catch(...)
        {
        }
    }
}





// *****************************************************************************
//   returns true if the passed in file name passes through the filter
//  and is of the type we're seeking - a file will return false here if we're
//  looking for directories
// *****************************************************************************
bool FileDialog::isAccepted(const fs::path& aPath) const
{
    if( (type_ == selectDirectories) && (aPath.leaf()[0] != '[') )
        return false;
    else if(aPath.leaf()[0] == '[')
        return true;

    std::string fileExt = fs::extension(aPath);
    size_t pos = 0;
    size_t dot = 0;
    while( (dot = filter().find('.', pos)) != std::string::npos )
    {
        size_t end = filter().find_first_of(";,(){}[]: \t\n\r\\/", dot);
        std::string filterExt = filter().substr(dot, end-dot);

        if ((filterExt == ".*") || (fileExt == filterExt))
        return true;
        dot++;
        pos = end;
    }
    return false;
}
