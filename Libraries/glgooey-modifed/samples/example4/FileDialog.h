// __________________________________________________________________________________________________
//    GLGooey Graphical User Interface for OpenGL
//    Copyright (c) 2004 Niel Waldren
//
// This software is provided 'as-is', without any express or implied warranty. In no event will
// the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose, including commercial
// applications, and to alter it and redistribute it freely, subject to the following restrictions:
//
//     1. The origin of this software must not be misrepresented; you must not claim that you
//        wrote the original software. If you use this software in a product, an acknowledgment
//        in the product documentation would be appreciated but is not required.
//
//     2. Altered source versions must be plainly marked as such, and must not be misrepresented
//        as being the original software.
//
//     3. This notice may not be removed or altered from any source distribution.
//
// __________________________________________________________________________________________________

#ifndef FILEDIALOG__H
#define FILEDIALOG__H

#include "mmgr/mmgr.h"

#include "glgooey/FrameWindow.h"

namespace boost
{
    namespace filesystem
    {
        class path;
    }
}


namespace Gooey
{

class StaticText;
class ComboBox;
class Button;
class ListBox;
class Panel;

// *****************************************************************************
/*! \class FileDialog
    \brief A file/directory selection dialog

    This class provides a dialog which can be used to select files or 
    directories. The dialog is a frame window containg various controls. The 
    file mechanisms behind this class are unfortunately not the same for 
    each platform (lack of posix support on Windows). But things should 
    work on Windows and all systems that support posix file handling. 
*/
// *****************************************************************************
class FileDialog : public FrameWindow
{
    // *************************************************************************
    //! \name Internal Data Types
    // *************************************************************************
    //@{
public:
    //! Array of strings
    typedef std::vector<std::string> Strings;

    //! The dialog can be used to select either files or directories
    enum Type { selectFiles, selectDirectories };
    //@}


    // *************************************************************************
    //! \name Construction / Destruction
    // *************************************************************************
    //@{
public:
    //! \brief Constructor
    //! 
    //! \param title the title of the frame window which is the dialog
    //! \param directory the directory to start searching from. 
    //!           Use "" or "." for the current working directory.
    //!           Use "/" for the root directory.
    //! \param filter an array of filters which may be used to filter the files
    //!           shown in the list (an empty list indicates that all files
    //!           should be shown).
    //! \param type select either file or directory selection using this value
    FileDialog(const std::string& title, const std::string& directory,
        Type type = selectFiles, const Strings& filter = Strings());

    //! Destructor
    virtual ~FileDialog();
    //@}


    // *************************************************************************
    //! \name Signals
    // *************************************************************************
    //@{
public:
    //! emitted when the "OK" button is pressed
    Signal0 okPressed;

    //! emitted when the "Cancel" button is pressed
    Signal0 cancelPressed;

    //! \brief emitted when a file is selected. The passed in string is the name of
    //! the selected file
    Signal1<const std::string&> fileSelected;

    //! \brief emitted when the current directory changes. The passed in string is
    //! the name of the path up to and including the currently selected
    //! directory
    Signal1<const std::string&> directoryChanged;
    //@}


    // *************************************************************************
    //! \name Public Interface
    // *************************************************************************
    //@{
public:
    //! returns the full path to the selected file
    std::string fullPath() const;

    //! returns the name of the selected directory
    std::string directoryName() const;

    //! returns the name of the selected file
    std::string fileName() const;

    //! returns the name of the selected filter
    std::string filter() const;

    //! react to the directory changing in the directory combo box
    FileDialog& onDirectoryChanged(const std::string& newDir);

    //! react to the filter changing in the filter combo box
    FileDialog& onFilterChanged(const std::string& newFilter);

    //! react to the selection changing in the list box
    FileDialog& onSelectionChanged(int index);

    //! react to the "up" button being pressed
    FileDialog& onUpPressed();

    //! react to the "next" button being pressed
    FileDialog& onNextPressed();

    //! react to the "previous" button being pressed
    FileDialog& onPreviousPressed();

    //! react to the "OK" button being pressed
    FileDialog& onOKPressed();

    //! react to the "Cancel" button being pressed
    FileDialog& onCancelPressed();

    //@}



    // *************************************************************************
    //! \name Implementation
    // *************************************************************************
    //@{
private:
    StaticText* directoryLabel_;
    StaticText* fileNameLabel_;
    StaticText* filterLabel_;

    ComboBox* directoryCombo_;
    ComboBox* filterCombo_;
    ComboBox* fileNameCombo_;

    Button* goButton_;
    Button* upButton_;
    Button* nextButton_;
    Button* previousButton_;
    Button* okButton_;
    Button* cancelButton_;

    ListBox* listBox_;

    Panel* panel_;
public:
    struct Entry
    {
        std::string name;
        bool        isDirectory;
        Entry(const std::string& aName, bool isDir) : name(aName), isDirectory(isDir) {}
        Entry(const Entry& anEntry) { (*this) = anEntry; }
        Entry& operator = (const Entry& anEntry)
        { name = anEntry.name; isDirectory = anEntry.isDirectory; return *this; }
        bool operator == (const Entry& anEntry) const
        { return ((name == anEntry.name) && (isDirectory == anEntry.isDirectory)); }
        bool operator != (const Entry& anEntry) const { return (!((*this) == anEntry)); }
    };
    typedef std::vector<Entry> Entries;
private:
    //! retrieves the contents of the passed in directory as an array of Entry objects
    void getDirectoryContents(const std::string& directory, Entries& entries);

    //! returns true if the passed in file name passes through the filter (i.e. is accepted)
    bool isAccepted(const boost::filesystem::path& aPath) const;

    Type type_;
    //@}



};

} // namespace

#endif
