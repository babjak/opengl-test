// __________________________________________________________________________________________________
//    GLGooey Graphical User Interface for OpenGL
//    Copyright (c) 2004 Niel Waldren
//
// This software is provided 'as-is', without any express or implied warranty. In no event will
// the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose, including commercial
// applications, and to alter it and redistribute it freely, subject to the following restrictions:
//
//     1. The origin of this software must not be misrepresented; you must not claim that you
//        wrote the original software. If you use this software in a product, an acknowledgment
//        in the product documentation would be appreciated but is not required.
//
//     2. Altered source versions must be plainly marked as such, and must not be misrepresented
//        as being the original software.
//
//     3. This notice may not be removed or altered from any source distribution.
//
// __________________________________________________________________________________________________




// *********************************************************************
//  a small app demonstrating the use of the file dialog
// *********************************************************************

#include "mmgr/nommgr.h"

#include <cstdlib>
#include <GL/glut.h>

#include <iostream>

#include "mmgr/mmgr.h"


#include "glgooey/core/StandardException.h"
#include "glgooey/core/Rectangle.h"
#include "glgooey/core/XMLInputArchive.h"
#include "glgooey/core/XMLOutputArchive.h"

#include "glgooey/WindowManager.h"
#include "glgooey/PropertyScheme.h"
#include "glgooey/StaticText.h"
#include "glgooey/Button.h"
#include "glgooey/FrameWindow.h"
#include "glgooey/Panel.h"
#include "glgooey/TimeManager.h"

#include "FileDialog.h"


using namespace Gooey;

int main_window;





class Console : public FrameWindow
{
public:
    Console() :
        FrameWindow(Core::Rectangle(0, 0, 450, 100), 0, "File Dialog Example"),
        panel_(0),
        fileLabel_(new StaticText(panel_, "")),
        button_(new Button(panel_, "...")),
        fileDialog_(0)
    {
        panel_ = new Panel(Core::Rectangle(10, 10, 20, 20), this);

        enableMovement();

        // resize the button and the label
        fileLabel_->setSize(Core::Vector2(330, 30));
        button_->setSize(Core::Vector2(40, 30));

        // wire up the signal/slot connection
        button_->pressed.connect(this, &Console::buttonPressed);

        // put the controls in the panel
        (*panel_)
            .addChildWindow(fileLabel_)
            .addChildWindow(button_)
            ;

        // put the panel in the frame window
        setClientWindow(panel_);

        // arrange the panel's children according to the default flow layouter
        panel_->arrangeChildren();
    }

    virtual ~Console()
    {
        // delete the windows;
        deleteFileDialog();
        delete panel_;
        delete fileLabel_;
        delete button_;
    }


public:
    // this is the slot that responds to the "..." button being pressed
    void buttonPressed()
    {
        deleteFileDialog();
        std::vector<std::string> filter;
        filter.push_back("GLGooey skin files, *.xml");
        fileDialog_ = new FileDialog("Open File", "", FileDialog::selectFiles, filter);
        fileDialog_->setPosition(Core::Vector2(10, 110));
        fileDialog_->okPressed.connect(this, &Console::fileNameChanged);
        fileDialog_->enableMovement();

        WindowManager::instance().addWindow(fileDialog_);
    }


    // this is the slot that responds to the "ok" button in the file dialog being pressed
    void fileNameChanged()
    {
        fileLabel_->setText(fileDialog_->fileName());
        if(fileDialog_->fullPath().find(".xml") != std::string::npos)
            WindowManager::instance().loadPropertyScheme(fileDialog_->fullPath());
        const Core::Vector2 sz = WindowManager::instance().applicationSize();
        WindowManager::instance().applicationResized(int(sz.x()), int(sz.y()));
    }
private:
    //! Convenience method for removing and deleting the file dialog
    void deleteFileDialog()
    {
        if(fileDialog_ != 0)
        {
            WindowManager::instance().removeWindow(fileDialog_);
            delete fileDialog_;
            fileDialog_ = 0;
        }
    }

    // the controls
    Panel*       panel_;
    StaticText*  fileLabel_;
    Button*      button_;
    FileDialog*  fileDialog_;
};



Console* console = 0;



// *************************************************************************
//  redraw in idle time
// *************************************************************************
void myGlutIdle( void )

{
    if ( glutGetWindow() != main_window ) 
        glutSetWindow(main_window);  
    
    glutPostRedisplay();
}



// *************************************************************************
//   this is where most of the integration work is done
//  myGlutMouse, myGlutMotion, myGlutKey, myGlutSpecial all send
//  their respective messages on to the window manager
// *************************************************************************
void myGlutMouse(int button, int state, int x, int y)
{
    switch(button)
    {
    case GLUT_LEFT_BUTTON:
        if(state == GLUT_UP)
            WindowManager::instance().onLeftButtonUp(x, y);
        else
            WindowManager::instance().onLeftButtonDown(x, y);
        break;
    case GLUT_MIDDLE_BUTTON:
        if(state == GLUT_UP)
            WindowManager::instance().onMiddleButtonUp(x, y);
        else
            WindowManager::instance().onMiddleButtonDown(x, y);
        break;
    case GLUT_RIGHT_BUTTON:
        if(state == GLUT_UP)
            WindowManager::instance().onRightButtonUp(x, y);
        else
            WindowManager::instance().onRightButtonDown(x, y);
        break;
    }
}



void myGlutMotion(int x, int y)
{
    WindowManager::instance().onMouseMove(x, y);
}


void myGlutKey(unsigned char key, int /*x*/, int /*y*/)
{
    WindowManager::instance().onChar(key);
}

void myGlutSpecial(int key, int /*x*/, int /*y*/)
{
    WindowManager::instance().onKeyDown(key);
}


bool isShiftPressed()
{
    return (glutGetModifiers() & GLUT_ACTIVE_SHIFT) != 0;
}

bool isAltPressed()
{
    return (glutGetModifiers() & GLUT_ACTIVE_ALT) != 0;
}

bool isCtrlPressed()
{
    return (glutGetModifiers() & GLUT_ACTIVE_CTRL) != 0;
}












// *************************************************************************
//   We're just going to draw a wireframe object in the center of the screen
//  so when a resize happens we reset the projection - not forgetting to
//  let the window manager know that the window it's running in has been
//  resized
// *************************************************************************
void myGlutReshape( int x, int y )
{
    WindowManager::instance().applicationResized(x, y);

    glViewport( 0, 0, x, y );
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(90, 1, 1, 100);
    gluLookAt(2, 2, 2, 0, 0, 0, 0, 1, 0);
    glMatrixMode(GL_MODELVIEW);             
    glLoadIdentity();

    glutPostRedisplay();
}




// *************************************************************************
//  the once per frame update
// *************************************************************************
void myGlutDisplay( void )
{
    // ------------------------ this stuff is initialization ----------------------

    // need this for 3d stuff in the background
    static GLUquadricObj* quadObj = 0;
    if(quadObj == 0)
    {
        quadObj = gluNewQuadric();
        gluQuadricDrawStyle(quadObj, GLU_LINE);
    }

    // ---------------------- This is where the frame starts --------------------

    // clear the screen
    glClearColor( 0.2, 0.2, 0.2, 1.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    // draw a wireframe cone
    glLoadIdentity();
    glRotatef(TimeManager::systemTime()*10.0, 1, 0, 0);
    glColor4f( 1, 1, 1, 1 );
    gluCylinder (quadObj, 1.6, 0.01, 2.5, 16, 16);

    // this must be called once per frame
    WindowManager::instance().update();

    // swap the buffers
    glutSwapBuffers();
}


void shutDown()
{
    WindowManager::instance().removeWindow(console);

    delete console;

    WindowManager::instance().terminate();
}



// *************************************************************************
//  main
// *************************************************************************
int main(int argc, char* argv[])
{
    atexit(shutDown);

    // *********************************************************************
    //   Initialize GLUT and create window
    // *********************************************************************

    glutInit(&argc, argv);
    glutInitDisplayMode( GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH );
    glutInitWindowPosition( 50, 50 );
    glutInitWindowSize( 640, 480 );

    main_window = glutCreateWindow( "Gooey Example4" );

    glutDisplayFunc ( myGlutDisplay );
    glutReshapeFunc ( myGlutReshape );
    glutIdleFunc    ( myGlutDisplay );
    glutMouseFunc   ( myGlutMouse   );
    glutKeyboardFunc( myGlutKey     );
    glutSpecialFunc ( myGlutSpecial );
    glutPassiveMotionFunc ( myGlutMotion  );

    glutMotionFunc  ( myGlutMotion  );

    WindowManager::instance().initialize("../../samples/data/accid___.ttf", isShiftPressed, isAltPressed, isCtrlPressed);

    console = new Console;
    WindowManager::instance().addWindow(console);

    try
    {
        glutMainLoop();
    }
    catch(std::exception& e)
    {
        std::cout << "Exception: " << e.what() << std::endl;
        std::cin.get();
    }


    return 0;
}
