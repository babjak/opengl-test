// __________________________________________________________________________________________________
//    GLGooey Graphical User Interface for OpenGL
//    Copyright (c) 2004 Niel Waldren
//
// This software is provided 'as-is', without any express or implied warranty. In no event will
// the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose, including commercial
// applications, and to alter it and redistribute it freely, subject to the following restrictions:
//
//     1. The origin of this software must not be misrepresented; you must not claim that you
//        wrote the original software. If you use this software in a product, an acknowledgment
//        in the product documentation would be appreciated but is not required.
//
//     2. Altered source versions must be plainly marked as such, and must not be misrepresented
//        as being the original software.
//
//     3. This notice may not be removed or altered from any source distribution.
//
// __________________________________________________________________________________________________


#include <GL/glut.h>

#include "mmgr/mmgr.h"

#include "glgooey/core/StandardException.h"
#include "glgooey/core/Rectangle.h"

#include "glgooey/WindowManager.h"
#include "glgooey/PropertyScheme.h"
#include "glgooey/FrameWindow.h"
#include "glgooey/Button.h"
#include "glgooey/EditField.h"
#include "glgooey/ListBox.h"
#include "glgooey/Panel.h"
#include "glgooey/TimeManager.h"
#include "glgooey/BoxLayouter.h"
#include "glgooey/GridLayouter.h"
#include "glgooey/FlowLayouter.h"

#include <iostream>

using namespace Gooey;

int main_window;

//  This is the base class for the example panels. It's a panel with a few buttons in it
// arranged as specified by the layouter passed in to the constructor
class ExamplePanel : public Panel
{
public:
    ExamplePanel(const Core::Rectangle& aRectangle, BaseLayouter* aLayouter) :
        Panel(aRectangle, 0, "Console"),
        button1_(0, ""),
        button2_(0, ""),
        button3_(0, "3"),
        button4_(0, "4"),
        button5_(0, "5")
    {
        button1_.setSize(Core::Vector2(40, 40));
        button1_.loadAppearance("../../samples/data/example2.xml", "onOffButton");

        button2_.setSize(Core::Vector2(40, 40));
        button2_.loadAppearance("../../samples/data/example2.xml", "leftButton");

        button3_.setSize(Core::Vector2(40, 40));
        button4_.setSize(Core::Vector2(40, 40));
        button5_.setSize(Core::Vector2(40, 40));

        setLayouter(aLayouter);

        addChildWindow(&button1_);
        addChildWindow(&button2_);
        addChildWindow(&button3_);
        addChildWindow(&button4_);
        addChildWindow(&button5_);

        arrangeChildren();
    }


private:
    // the controls
    Button button1_;
    Button button2_;
    Button button3_;
    Button button4_;
    Button button5_;
};


//  The flow example is an example panel which uses the flow layouter - per default panels use
// a left-justified flow layouter. Here a centered flow layouter is used.
class FlowExample : public ExamplePanel
{
public:
    FlowExample(const Core::Rectangle& aRectangle) :
        ExamplePanel(aRectangle, new FlowLayouter(FlowLayouter::center))
    {
    }
};


// The grid example is an example panel which uses the grid layouter - 3 rows, two columns and
// spacing of 5 pixels both horizontally and vertically
class GridExample : public ExamplePanel
{
public:
    GridExample(const Core::Rectangle& aRectangle) :
        ExamplePanel(aRectangle, new GridLayouter(3, 2, 5, 5))
    {
    }
};


// The box example is an example panel which uses the box layouter. The justification and whether
// the layouter should respect the child window sizes can be specified by client code. The example
// shows two box examples, one with a horizontal justification that respects child windows sizes
// and one with a vertical justification that resizes its child windows if necessary
class BoxExample : public ExamplePanel
{
public:
    BoxExample(const Core::Rectangle& aRectangle, BoxLayouter::Justification aJustification, bool respectSizes) :
        ExamplePanel(aRectangle, new BoxLayouter(aJustification, 5.0f, respectSizes))
    {
    }
};


// The main panel is a small panel at the bottom of the window which contains the previous and next
// buttons and the name of the layouter currently in use. This class also deals with deciding
// which examples to show
class MainPanel : public Panel
{
public:
    enum Type { flow = 0, box, grid, numberOfTypes };

    struct Entry
    {
        Type type_;
        std::string name_;
    };

public:
    MainPanel() :
        Panel(Core::Rectangle(40, 400, 650, 450), 0, "BottomPanel"),
        typeTracker_(0),
        leftButton_(0, ""),
        rightButton_(0, ""),
        textField_(0, "Box Layouter"),
        topBox_(Core::Rectangle(50, 50, 450, 100), BoxLayouter::horizontal, true),
        bottomBox_(Core::Rectangle(250, 120, 350, 350), BoxLayouter::vertical, true),
        grid_(Core::Rectangle(250, 100, 350, 300)),
        flow_(Core::Rectangle(250, 100, 350, 300))
    {
        entries_[flow].type_ = flow;
        entries_[flow].name_ = "Flow Layouter";
        entries_[grid].type_ = grid;
        entries_[grid].name_ = "Grid Layouter";
        entries_[box].type_  = box;
        entries_[box].name_  = "Box Layouter";

        loadAppearance("../../samples/data/example2.xml", "emptyPanel");

        leftButton_.setSize(Core::Vector2(40, 40));
        leftButton_.loadAppearance("../../samples/data/example2.xml", "leftButton");

        rightButton_.setSize(Core::Vector2(40, 40));
        rightButton_.loadAppearance("../../samples/data/example2.xml", "rightButton");

        leftButton_.pressed.connect(this, &MainPanel::previousPressed);
        rightButton_.pressed.connect(this, &MainPanel::nextPressed);

        textField_.setSize(Core::Vector2(400, 40));
        textField_.loadAppearance("../../samples/data/example2.xml", "mainTextField");

        BoxLayouter* layouter = new BoxLayouter(BoxLayouter::horizontal, 25.0f, true);
        setLayouter(layouter);

        addChildWindow(&leftButton_);
        addChildWindow(&textField_);
        addChildWindow(&rightButton_);

        arrangeChildren();

        update();
    }


private:
    void nextPressed()
    {
        typeTracker_ = (typeTracker_ + 1) % numberOfTypes;
        update();
    }

    void previousPressed()
    {
        typeTracker_ = (typeTracker_ == 0) ? numberOfTypes - 1 : typeTracker_ - 1;
        update();
    }

    void update()
    {
        switch(typeTracker_)
        {
        case 0:
            WindowManager::instance().removeWindow(&topBox_);
            WindowManager::instance().removeWindow(&bottomBox_);
            WindowManager::instance().removeWindow(&grid_);
            WindowManager::instance().addWindow(&flow_);
            break;
        case 1:
            WindowManager::instance().removeWindow(&topBox_);
            WindowManager::instance().removeWindow(&bottomBox_);
            WindowManager::instance().addWindow(&grid_);
            WindowManager::instance().removeWindow(&flow_);
            break;
        case 2:
            WindowManager::instance().addWindow(&topBox_);
            WindowManager::instance().addWindow(&bottomBox_);
            WindowManager::instance().removeWindow(&grid_);
            WindowManager::instance().removeWindow(&flow_);
            break;
        default:
            break;
        }
        textField_.setText(entries_[typeTracker_].name_);
    }

    // The typeTracker keeps track of which type is currently displayed
    int typeTracker_;

    // Helper
    Entry entries_[numberOfTypes];

    // the controls
    Button leftButton_;
    Button rightButton_;
    StaticText textField_;

    // the windows
    BoxExample topBox_;
    BoxExample bottomBox_;
    GridExample grid_;
    FlowExample flow_;
};




MainPanel* console = 0;


// *************************************************************************
//  redraw in idle time
// *************************************************************************
void myGlutIdle( void )

{
    if ( glutGetWindow() != main_window )
        glutSetWindow(main_window);

    glutPostRedisplay();
}



// *************************************************************************
//   this is where most of the integration work is done
//  myGlutMouse, myGlutMotion, myGlutKey, myGlutSpecial all send
//  their respective messages on to the window manager
// *************************************************************************
void myGlutMouse(int button, int state, int x, int y)
{
    switch(button)
    {
    case GLUT_LEFT_BUTTON:
        if(state == GLUT_UP)
            WindowManager::instance().onLeftButtonUp(x, y);
        else
            WindowManager::instance().onLeftButtonDown(x, y);
        break;
    case GLUT_MIDDLE_BUTTON:
        if(state == GLUT_UP)
            WindowManager::instance().onMiddleButtonUp(x, y);
        else
            WindowManager::instance().onMiddleButtonDown(x, y);
        break;
    case GLUT_RIGHT_BUTTON:
        if(state == GLUT_UP)
            WindowManager::instance().onRightButtonUp(x, y);
        else
            WindowManager::instance().onRightButtonDown(x, y);
        break;
    }
}



void myGlutMotion(int x, int y)
{
    WindowManager::instance().onMouseMove(x, y);
}


void myGlutKey(unsigned char key, int /*x*/, int /*y*/)
{
    WindowManager::instance().onChar(key);
}

void myGlutSpecial(int key, int /*x*/, int /*y*/)
{
    WindowManager::instance().onKeyDown(key);
}


bool isShiftPressed()
{
    return (glutGetModifiers() & GLUT_ACTIVE_SHIFT) != 0;
}

bool isAltPressed()
{
    return (glutGetModifiers() & GLUT_ACTIVE_ALT) != 0;
}

bool isCtrlPressed()
{
    return (glutGetModifiers() & GLUT_ACTIVE_CTRL) != 0;
}












// *************************************************************************
//   We're just going to draw a wireframe sphere in the center of the screen
//  so when a resize happens we reset the projection - not forgetting to
//  let the window manager know that the window it's running in has been
//  resized
// *************************************************************************
void myGlutReshape( int x, int y )
{
    WindowManager::instance().applicationResized(x, y);

    glViewport( 0, 0, x, y );
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(90, 1, 1, 100);
    gluLookAt(2, 2, 2, 0, 0, 0, 0, 1, 0);
    glMatrixMode(GL_MODELVIEW);             
    glLoadIdentity();

    glutPostRedisplay();
}




// *************************************************************************
//  the once per frame update
// *************************************************************************
void myGlutDisplay( void )
{
    // ------------------------ this stuff is initialization ----------------------

    // need this for 3d stuff in the background
    static GLUquadricObj* quadObj = 0;
    if(quadObj == 0)
    {
        quadObj = gluNewQuadric();
        gluQuadricDrawStyle(quadObj, GLU_LINE);
    }

    // ---------------------- This is where the frame starts --------------------

    // clear the screen
    glClearColor( 0.1, 0.1, 0.1, 1.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    // draw a wireframe cone
    glLoadIdentity();
    glRotatef(TimeManager::systemTime()*10.0, 1, 0, 0);
    glColor4f( 1, 1, 1, 1 );

    gluCylinder (quadObj, 1.6, 0.01, 2.5, 16, 16);

    // this must be called once per frame
    WindowManager::instance().update();

    // swap the buffers
    glutSwapBuffers();

}




void shutDown()
{
    WindowManager::instance().removeWindow(console);

    delete console;

    WindowManager::instance().terminate();
}




// *************************************************************************
//  main
// *************************************************************************
int main(int argc, char* argv[])
{
    atexit(shutDown);

    // *********************************************************************
    //   Initialize GLUT and create window  
    // *********************************************************************
    
    glutInit(&argc, argv);
    glutInitDisplayMode( GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH );
    glutInitWindowPosition( 50, 50 );
    glutInitWindowSize( 640, 480 );
    
    main_window = glutCreateWindow( "Gooey Example2" );

    glutDisplayFunc ( myGlutDisplay );
    glutReshapeFunc ( myGlutReshape );
    glutIdleFunc    ( myGlutDisplay );
    glutMouseFunc   ( myGlutMouse   );
    glutKeyboardFunc( myGlutKey     );
    glutSpecialFunc ( myGlutSpecial );
    glutPassiveMotionFunc ( myGlutMotion  );
    glutMotionFunc  ( myGlutMotion  );

    WindowManager::instance().initialize("../../samples/data/accid___.ttf", isShiftPressed, isAltPressed, isCtrlPressed);
    WindowManager::instance().propertyScheme().load("../../samples/data/redskin.xml");

    console = new MainPanel();
    WindowManager::instance().addWindow(console);

    try
    {
        glutMainLoop();
    }
    catch(std::exception& e)
    {
        std::cout << "Exception: " << e.what() << std::endl;
        std::cin.get();
    }

    return 0;
}
